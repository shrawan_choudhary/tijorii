<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OfferHistory extends Model
{
    protected $guarded = [];

    public function offer()
    {
    	# code...
    	return $this->hasOne('App\Model\Offer','id','task_id');
    }
    public function users()
    {
        # code...
        return $this->hasOne('App\User','id','uid');
    }
}
