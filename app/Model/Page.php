<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $guarded = [];
    protected $hidden = [
        'slug', 'id', 'created_at', 'updated_at'
    ];
}
