<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $hidden  = ['type'];

    protected $casts   = [
        'created_at'  => 'datetime:d/m/Y h:i A',
        'updated_at'  => 'datetime:d/m/Y h:i A',
    ];

    public function history()
    {
        return $this->hasOne('App\Model\OfferHistory', 'task_id', 'id');
    }
}
