<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $guarded = [];
    protected $hidden = [
        'id', 'created_at', 'updated_at'
    ];
}
