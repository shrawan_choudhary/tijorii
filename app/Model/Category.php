<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function offers() {
    	return $this->hasMany('App\Model\Survey', 'category', 'name');
    }

    public static function scopeSearch($query, $searchTerm) {
        return $query->where('name', 'like', '%' .$searchTerm. '%');
                     
    }
}
