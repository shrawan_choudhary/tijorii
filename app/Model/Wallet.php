<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model {
	// 
	// protected $table 		= "wallet";
    
    
    protected $guarded = [];

    protected $casts   = [
        'created_at'    => 'datetime:M d, Y h:i A',
        'updated_at'    => 'datetime:M d, Y h:i A',
    ];

    public function user()
    {
        # code...
        return $this->hasOne('App\User','id','uid');
    }
    public function offerhistory()
    {
        # code...
        return $this->hasOne('App\Modal\OfferHistory','id','task_id');
    }
}
