<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SurveyHistory extends Model {
	// 
    protected $guarded = [];
	protected $table 		= "offer_histories";
    
    

    public function survey()
    {
    	# code...
    	return $this->hasOne('App\Model\Survey','id','task_id');
    }
    public function user()
    {
    	# code...
    	return $this->hasOne('App\User','id','uid');
    }
}
