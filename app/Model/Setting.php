<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {
    protected $guarded = [];
    protected $hidden = [
        'id', 'created_at', 'updated_at'
    ];

    protected $casts = [
        'min_wallet_point' => 'double'
    ];
}
