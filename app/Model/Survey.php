<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    //
    protected $table 		= "offers";
    
    protected $guarded = [];
	protected $hidden  = ['type'];

    protected $casts   = [
        'created_at'  => 'datetime:d/m/Y h:i A',
        'updated_at'  => 'datetime:d/m/Y h:i A',
    ];



}
