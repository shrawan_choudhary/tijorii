<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use App\Model\Wallet;
use App\User;
use App\Model\Setting;

class UserController extends Controller
{
    public $successStatus = 200;
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function sendOtp(Request $request)
    {
        $setting = Setting::find(1);
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required|numeric|regex:/\d{10}/',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
            $otp = rand(100000, 999999);

            $message = urlencode("Your One Time Password (OTP) is {$otp} for {$setting->app_name}.");
            // print_r($setting);
            $sms_url = str_replace(["[MOBILE]", "[MESSAGE]"], [request('mobile'), $message], $setting->sms_api);
            // die;

            $sms = file_get_contents($sms_url);

            $user = User::where('login', request('mobile'));

            if ($user->count()) {
                $dataArr = [
                    'password'  => bcrypt($otp),
                ];
                User::updateOrCreate(['login' => request('mobile')], $dataArr);
            }

            $re = [
                'status'    => true,
                'message'   => 'OTP has been sent to ' . request('mobile'),
                'otp_code'  => $otp,
                'isExists'  => $user->count()
            ];
            $code = 200;
        }
        return response()->json($re, $code);
    }
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'mobile'    => 'required|numeric|regex:/\d{10}/|unique:users',
            'gender'    => 'required',
            'name'      => 'required'
        ]);


        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {


            if (!empty($request->referal_code)) {
                $user = User::where('referal_code', $request->referal_code)->first();

                if (!$user) {
                    return response()->json(['message' => 'Referal code is not matched.'], 401);
                }
            }

            $string    = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            $referCode = substr(str_shuffle($string), 0, 4);


            // $dataArr = [
            //     'login'         => request('mobile'),
            //     'password'      => 'DEFAULT',
            //     'name'          => request('name'),
            //     'mobile'        => request('mobile'),
            //     'email'         => request('email'),
            //     'referal_code'  => $referCode,
            //     'refered_by'    => request('referal_code'),
            //     'gender'        => request('gender'),
            // ];
            // $newUser    = User::create($dataArr);
            $obj = new User;
            $obj->login        = $request->mobile;
            $obj->password      = $request->password;
            $obj->name        = $request->name;
            $obj->mobile        = $request->mobile;
            $obj->email        = $request->email;
            $obj->referal_code        = $request->referal_code;
            // $obj->refered_by        = $request->refered_by;
            $obj->gender        = $request->gender;

            // $obj->image  = $request->$file->getClientOriginalName();

            $obj->save();


            $token      = $obj->createToken('cashBuddy')->accessToken;

            // Update Referal Code
            $referCode             .= sprintf($obj->id);
            
            $obj->referal_code  = $referCode;
            $obj->save();

            $message = 'Registration success.';
            $setting = Setting::find(1);
            
            if (!empty($user) && !empty($setting->refer_point)) {
                $walletArr = [
                    'amount'    => $setting->refer_point,
                    'type'      => 'Credit',

                    'remarks'   => $obj->name . ' has registered with referal code ' . $request->referal_code . '.',
                    'status'    => 'approved',
                ];

                $message .= " Refer points earned by you, check wallet history.";

                $user->wallet()->create($walletArr);
                

            }

            $re = [
                'status'    => true,
                'message'   => $message,
                'token'     => $token,
                'data'      => $obj
            ];
            $code = 200;
        }
        return response()->json($re, $code);
    }
    public function login()
    {

        $user = User::where('login', request('mobile'));
        

        if (empty(request('mobile') || empty(request('otp')))) {
            $re = [
                'status'    => false,
                'message'   => 'Required field(s) missing.',
                'isExists'  => 0,
            ];
            $code = 401;
        } elseif ($user->count() == 0) {
            $re = [
                'status'    => true,
                'message'   => 'User not registered',
                'isExists'  => 0,
            ];
            $code = 200;
        } elseif (Auth::attempt(['mobile' => request('mobile'), 'password' => request('otp')])) {
            $user   = Auth::user();
            $token  = $user->createToken('cashBuddy')->accessToken;

            $re = [
                'status'    => true,
                'message'   => 'You\'ve logged in successfully.',
                'token'     => $token,
                'isExists'  => 1,
            ];
            $code = 200;
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Unauthorised! Credentials not matched.',
            ];
            $code = 401;
        }
        return response()->json($re, $code);
    }
    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
}