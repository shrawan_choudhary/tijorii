<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

use App\Model\Wallet;
use App\Model\Setting;

class WalletController extends Controller
{
    public function index()
    {
        $lists = Wallet::where('uid', auth()->user()->id)->latest()->paginate(50);
        if ($lists->isEmpty()) {
            $re = [
                'status'    => false,
                'message'   => 'No records found.'
            ];
            $code = 401;
        } else {
            $re     = [
                "status" => true,
                "record" => $lists
            ];
            $code   = 200;
        }

        return response()->json($re, $code);
    }
    public function withdraw(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount'   => 'required|numeric'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
            $totalCredit        = Wallet::where('uid', auth()->user()->id)->where('type', 'Credit')->sum('amount');
            $totalDebit         = Wallet::where('uid', auth()->user()->id)->where('type', 'Debit')->sum('amount');

            $wallet_amt         = $totalCredit - $totalDebit;

            $setting            = Setting::find(1);

            if ($setting->min_wallet_point > $wallet_amt) {
                $re = [
                    'status'    => false,
                    'message'   => 'Min. wallet amount must be '.$setting->min_wallet_point.', You\'ve amount only '.$wallet_amt,
                    'wallet_amt'=> $wallet_amt,
                ];
                $code = 401;
            } else {
                $walletArr = [
                    'uid'       => auth()->user()->id,
                    'amount'    => $request->amount,
                    'type'      => 'Debit',
                    'remarks'   => auth()->user()->name.' has requested to withdraw his wallet amount.',
                    'wallet_amt'=> $wallet_amt,
                ];
                Wallet::create($walletArr);

                $re = [
                    'status'    => true,
                    'message'   => 'Withdrawal request has been sent, your account will be credited after approval.',
                    'wallet_amt'=> $wallet_amt,
                ];
                $code = 200;
            }
        }

        return response()->json($re, $code);
    }
}
