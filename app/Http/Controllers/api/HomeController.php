<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;

use App\Model\Setting;
use App\Model\Wallet;
use App\Model\DailyPoint;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $record             = Setting::find(1);

        $totalCredit        = Wallet::where('uid', auth()->user()->id)->where('type', 'Credit')->sum('amount');
        $totalDebit         = Wallet::where('uid', auth()->user()->id)->where('type', 'Debit')->sum('amount');
           
        $record->wallet_amt = $totalCredit - $totalDebit;

        $daily_points       = DailyPoint::where('uid', auth()->user()->id)->orderBy('day_count', 'ASC')->get();

        $re = [
            'status'        => true,
            'message'       => 'Data found',
            'data'          => $record,
            'profile'       => auth()->user(),
            'daily_points'  => $daily_points
        ];
        $code = 200;

        return response()->json($re, $code);
    }
}
