<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

use App\Model\OfferHistory;
use App\Model\Offer;
use App\Model\Wallet;


class OfferHistoryController extends Controller
{
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'task_id'   => 'required|numeric'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
            $arr = [
                'task_id'   => $request->task_id,
                'uid'       => auth()->user()->id,
            ];
            $ohistory = OfferHistory::updateOrCreate($arr, $arr);

            $re = [
                'status'    => true,
                'click_id'  => $ohistory->id,
                'message'   => 'Task sent to approval',
            ];
            $code = 200;
        }

        return response()->json($re, $code);
    }
    public function change_status(Request $request, $clickId = null)
    {
        // dd($clickId);
        $id = OfferHistory::whereRaw("md5(id) = '".$clickId."'")->firstOrFail();
        $d_o = Offer::findOrFail($id->task_id);
         
     // dd($d_o);
        $status=$id->status;
        $data = $id->update(["status" => "completed"]);
        
        if($status!="completed"){
         $s_data = new Wallet();
         $s_data->uid = $id->uid;
         $s_data->mobile_no = $id->uid;
         $s_data->offer = $d_o->name;
         $s_data->amount = $d_o->earn_price;
         $s_data->type = 'Credit';
         $s_data->remarks = '₹'. $d_o->earn_price.' added for '.  $d_o->name;
        $s_data->save();
        }
        
        $re = [
            'status'    => true,
            'id'        => $id->id,
            'message'   => 'Task has been completed',
        ];
        return response()->json($re, 200);
    }
}
