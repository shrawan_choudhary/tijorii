<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\DailyPoint;

class DailyPointsController extends Controller
{
    public function earn_point()
    {
        $user       = auth()->user();
        $dailyCount = ($user->daily_login_no + 1) > 7 ? 1 : $user->daily_login_no + 1;

        $currentDate = date('Y-m-d', time());
        $yesterDate  = date('Y-m-d', strtotime("-1 days"));

        if (!empty($user->last_checkin) && $user->last_checkin != $yesterDate) {
            $dailyCount = 1;
        }

        if (empty($user->last_checkin) || $user->last_checkin != $currentDate) {
            $user->daily_login_no = $dailyCount;
            $user->last_checkin   = $currentDate;
            $user->save();

            $points = DailyPoint::find(14);
            
            $points->daily_login_no = auth()->user()->daily_login_no;

            $walletAmt = $points->{'day'.$dailyCount};

            $arr = [
                'amount'    => $walletAmt,
                'type'      => 'Credit',
                'remarks'   => 'Daily point earned for day '.$dailyCount,
                'status'    => 'approved'
            ];
            $user->wallet()->create($arr);

            $points->status  = true;
            $points->message = 'You\'ve earned '.$walletAmt.' points for day '.$dailyCount;

            return response()->json($points, 200);
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Already checked in!!'
            ];
            return response()->json($re, 401);
        }
    }
}
