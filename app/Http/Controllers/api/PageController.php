<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Page;
use Illuminate\Support\Facades\Auth;
use Validator;

class PageController extends Controller
{
    public $successStatus = 200;

    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'slug'  => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
            $code = 401;
        } else {
            $record = Page::where('slug', request('slug'))->first();

            if (!empty($record->id)) {
                $re = [
                    'status'    => true,
                    'message'   => 'Data found',
                    'data'      => $record,
                ];
                $code = 200;
            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Invalid Parameters.'
                ];
                $code = 401;
            }
        }
        return response()->json($re, $code);
    }
}
