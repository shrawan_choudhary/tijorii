<?php
namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Offer;

class OfferController extends Controller
{
    public function index()
    {
        $currentRoute   = \Request::route()->getName();
        $type           = $currentRoute == "offer" ? 'offer list' : 'survey';
        $lists          = Offer::withCount(['history' => function($q) {
            $q->where('uid', auth()->user()->id)->where('status' ,'completed');
        }])->where('type', $type)->paginate(10);
        if ($lists->isEmpty()) {
            $re = [
                'status'    => false,
                'message'   => 'No record(s) found.',
            ];
            $code   = 401;
        } else {
            foreach ($lists as $key => $list) {
                $folderName = $list->type == "survey" ? "survey" : "offer-list";
                $lists[$key]->image = !empty($list->image) ? url("imgs/{$folderName}/".$list->image) : "";
                $lists[$key]->is_attempted = $list->history_count;
            }
            $re = [];
            $re['record']   = $lists;
            $re['status']   = true;
            $code           = 200;
        }
        return response()->json($re, $code);
    }
    public function info( $id)
    {
        $offer=Offer::withCount(['history' => function($q) {
            $q->where('uid', auth()->user()->id)->where('status', 'completed');
        }])->findOrFail($id);
        if (!$offer) {
            $re = [
                'status'    => false,
                'message'   => 'Not found.'
            ];
            $code   = 404;
        } else {
            $folderName = $offer->type == "survey" ? "survey" : "offer-list";
            $offer->image = !empty($offer->image) ? url("imgs/{$folderName}/".$offer->image) : "";
            $offer->is_attempted = $offer->history_count;
            $re = [];
            $re['record']   = $offer;
            $re['status']   = true;
            $code           = 200;
        }
        return response()->json($re, $code);
    }
    public function history()
    {
        $currentRoute   = \Request::route()->getName();
        $type           = $currentRoute == "offer_history" ? 'offer list' : 'survey';
        $lists          = Offer::with('history')->where('type', $type)->whereHas('history', function ($q) {
            $q->where('uid', auth()->user()->id);
        })->paginate(10);
        if ($lists->isEmpty()) {
            
            $re = [
                'status'    => false,
                'message'   => 'No record(s) found.',
            ];
            $code   = 401;
        } else {
            foreach ($lists as $key => $list) {
                $folderName = $list->type == "survey" ? "survey" : "offer-list";
                $lists[$key]->image = !empty($list->image) ? url("imgs/{$folderName}/".$list->image) : "";
            }
            $re = [];
            $re['record']   = $lists;
            $re['status']   = true;
            $code   =  200;
        }
        return response()->json($re, $code);
    }
}
