<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Faq;
use Illuminate\Support\Facades\Auth;
use Validator;

class FaqController extends Controller
{
    public $successStatus = 200;

    public function index(Request $request)
    {
        $records = Faq::paginate(30);

        if (!$records->isEmpty()) {
            $re = [
                'status'    => true,
                'message'   => 'Data found',
                'data'      => $records,
            ];
            $code = 200;
        } else {
            $re = [
                'status'    => false,
                'message'   => 'No record(s) found.'
            ];
            $code = 401;
        }

        return response()->json($re, $code);
    }
}
