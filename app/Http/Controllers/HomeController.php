<?php

namespace App\Http\Controllers;
use App\Model\Pages;
use App\Model\Setting;
use App\Model\Enquiry;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller as BaseController;

class HomeController extends BaseController {
    public function index() 
    {	
        $setting = Setting::findOrFail(1);
    	return view('frontend.inc.homepage',compact('setting'));
    }
    public function contact() 
    {	
        $setting = Setting::findOrFail(1);
    	return view('frontend.inc.contact',compact('setting'));
    }
    public function Pages($slug)
    {
        $pages = Pages::where('slug',$slug)->first();
        
        $data = compact('pages');
        return view('frontend.inc.pages',$data);
    }
    public function enquiry(Request $request){
        $input = $request->all();
        $obj = new Enquiry();
        $obj->name = $request->name;
        $obj->email = $request->email;
        $obj->subject = $request->subject;
        $obj->message = $request->message;
        
        if($obj->save()){
               return response()->json(['status'=>1]);
         }
         else{
                return response()->json(['status'=>0]);
         }
    }
}
