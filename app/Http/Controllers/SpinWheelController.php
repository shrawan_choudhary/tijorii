<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Model\SpinWheel;
use App\Model\DailyPoint;
use App\Model\Wallet;

class SpinWheelController extends Controller
{
    public function index(User $user)
    {
        $spin    = SpinWheel::find(1);
        $asc_arr = $spinArr = [
            $spin->number1,
            $spin->number2,
            $spin->number3,
            $spin->number4,
            $spin->number5,
            $spin->number6,
            $spin->number7,
            $spin->number8,
            $spin->number9,
            $spin->number10,
        ];
        asort($asc_arr);
        $asc_arr = array_values($asc_arr);

        $currentDate = date('Y-m-d');
        $yesterDate  = strtotime('-1 days');

        $last_daily_point = DailyPoint::where('uid', $user->id)->orderBy('day_count', 'DESC')->first();

        $completed  = false;
        $new_day    = 1;
        $lastNumber = 0;
        if (!empty($last_daily_point->id)) {
            $rules = false;
            $new_day     = 0;
            $currentDate = date('Y-m-d');
            $yesterDate  = strtotime('-1 days');

            $lastDate    = $last_daily_point->created_at->format("Y-m-d");
            if ($currentDate == $lastDate) {
                $completed = true;
            }
            $datediff = $yesterDate - strtotime($last_daily_point->created_at);
            $datediff = round($datediff / (60 * 60 * 24));

            $lastNumber = $last_daily_point->point;

            $new_day  = $last_daily_point->day_count + 1;
            if (($datediff >= 1 || $last_daily_point->day_count == 7) && $lastDate != $currentDate) {
                $rules   = true;
            }

            if ($rules) {
                $new_day = 1;
                DailyPoint::where('uid', $user->id)->delete();
            }
        }

        $data = compact('user', 'spinArr', 'asc_arr', 'new_day', 'completed', 'lastNumber');
        return view('spin_wheel', $data);
    }
    public function save(Request $request)
    {
        $arr = $request->except('_token');
        $point = DailyPoint::create($arr);

        if ($arr['day_count'] == 7) {
            $amount = DailyPoint::where('uid', $arr['uid'])->sum('point');
            $walletArr = [
                'uid'       => $arr['uid'],
                'amount'    => $amount,
                'type'      => 'Credit',
                'remarks'   => 'Total '.$amount.' points added by daily spin.',
                'status'    => 'approved'
            ];
            Wallet::create($walletArr);
        }

        return response()->json(['id' => $point->id], 200);
    }
}
