<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\SpinWheel;




class SpinController extends Controller {
    public function index(request $request) {

        $lists = SpinWheel::latest()->paginate();
        


        $data = compact( 'lists' ); // Variable to array convert
        return view('backend.inc.spin.index', $data);
    }


   
    public function edit( Request $request, $id ) {

        $edit = SpinWheel::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();
        
        return view('backend.inc.spin.edit', compact('edit'));

    }
    public function editData( Request $request, $id ) {
        $rules = [
            'number1'  => 'required',
            'number2'  => 'required',
            'number3'  => 'required',
            'number4'  => 'required',
            'number5'  => 'required',
            'number6'  => 'required',
            'number7'  => 'required',
            'number8'  => 'required',
            'number9'  => 'required',
            'number10'  => 'required'
        ];
        $request->validate( $rules );
        

        $obj = SpinWheel::findOrFail( $id );
         $obj->number1        = $request->number1;
        $obj->number2  = $request->number2;
        $obj->number3  = $request->number3;
        $obj->number4  = $request->number4;
        $obj->number5  = $request->number5;
        $obj->number6  = $request->number6;
        $obj->number7  = $request->number7;
        $obj->number8  = $request->number8;
        $obj->number9  = $request->number9;
        $obj->number10 = $request->number10;

        $obj->save();

        return redirect()->back()->with('success', 'Success! A record has been updated.');
    }
    

// SELECT * FROM `news` WHERE `news_id` = 1 or `news_id` = 2
    // News::where('news_id', 1)->orWhere('news_id', 2)->get();
}


   
    
    
   
   

