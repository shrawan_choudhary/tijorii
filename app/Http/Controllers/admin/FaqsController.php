<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Faq;
class FaqsController extends Controller{
   
    public function index(request $request){
        //
        $query = Faq::latest();

        if( !empty( $request->question ) ) {
            $query->where('question', 'LIKE', '%'.$request->question.'%');
        }
        $lists1 = $query->paginate(20);

        $data = compact( 'lists1' ); // Variable to array convert
        return view('backend.inc.faqs.index', $data);
    }

    

   
    public function add()
    {
        //
        return view('backend.inc.faqs.add');
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'question'        => 'required',
            'answer'        => 'required'
            
            
        ];
        $request->validate( $rules );
        // $file = $request->file('image');
        // $destinationPath = 'public/imgs/shop/';
        // $file->move($destinationPath,$file->getClientOriginalName());

        
           

        

        $obj = new Faq;
        $obj->question        = $request->question;
        $obj->answer        = $request->answer;
        
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return redirect( url('admin-control/faqs/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Faq::findOrFail( $id )->toArray();
        $request->replace($edit);
        $request->flash();

        return view('backend.inc.faqs.edit');
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            'question'        => 'required',
            'answer'        => 'required'
            
        ];
        $request->validate( $rules );
        $file = $request->file('image');
        // $destinationPath = 'public/imgs/shop/';
        // $file->move($destinationPath,$file->getClientOriginalName());
        // $filename    = $file->getClientOriginalName();
        // $image_resize = Image::make($file->getRealPath());              
        // $image_resize->resize(300, 300);
        // $image_resize->save(public_path('imgs/survey/' .$filename));

        $obj = Faq::findOrFail( $id );
        $obj->question        = $request->question;
        $obj->answer        = $request->answer;
        
        // $obj->image  = $request->$file->getClientOriginalName();
        $obj->save();

        return redirect( url('admin-control/faqs') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
        $remove = Faq::where('id',$id)->delete();
        return redirect( url('admin-control/faqs') )->with('success', 'Success! A record has been deleted.');   
    }

    public function removeMultiple(Request $request)
    {
        $check = $request->check; // input type="checkbox" name="check[]"
        Faq::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

        return redirect()->back()->with('success', 'Item(s) removed.');
    }

   
}
