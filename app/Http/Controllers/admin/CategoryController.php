<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Category;
class CategoryController extends Controller{
   
    public function index(request $request){
        //
        $query = Category::withCount('offers')->latest();

        if( !empty( $request->name ) ) {
            $query->where('name', 'LIKE', '%'.$request->name.'%');
        }
        $lists1 = $query->paginate(20);

        $data = compact( 'lists1' ); // Variable to array convert
        return view('backend.inc.category.index', $data);
    }
    

    

   
    public function add()
    {
        //
        return view('backend.inc.category.add');
    }

    
    public function addData(Request $request)
    {
        //
        $rules = [
            'name'        => 'required'
            
            
        ];
        $request->validate( $rules );
        // $file = $request->file('image');
        // $destinationPath = 'public/imgs/shop/';
        // $file->move($destinationPath,$file->getClientOriginalName());

        
           

        

        $obj = new Category;
        $obj->name        = $request->name;
        
        
        // $obj->image  = $request->$file->getClientOriginalName();
        
        $obj->save();

        return redirect( url('admin-control/category/') )->with('success', 'Success! New record has been added.');
    }

   
    public function edit(Request $request,$id)
    {
        //
        $edit = Category::findOrFail( $id )->toArray();
        $request->replace($edit);
        $request->flash();

        return view('backend.inc.category.edit');
    }

    
    public function editData(Request $request, $id)
    {
        //
        $rules = [
            'name'        => 'required'
            
        ];
        $request->validate( $rules );
        // $file = $request->file('image');
        // $destinationPath = 'public/imgs/shop/';
        // $file->move($destinationPath,$file->getClientOriginalName());
        // $filename    = $file->getClientOriginalName();
        // $image_resize = Image::make($file->getRealPath());              
        // $image_resize->resize(300, 300);
        // $image_resize->save(public_path('imgs/survey/' .$filename));

        $obj = Category::findOrFail( $id );
        $obj->name        = $request->name;
        
        
        // $obj->image  = $request->$file->getClientOriginalName();
        $obj->save();

        return redirect( url('admin-control/category') )->with('success', 'Success! A record has been updated.');
    }
     public function remove(  $id ){
        $remove = Category::where('id',$id)->delete();
        return redirect( url('admin-control/category') )->with('success', 'Success! A record has been deleted.');   
    }

    public function removeMultiple(Request $request)
    {
        $check = $request->check; // input type="checkbox" name="check[]"
        Category::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

        return redirect()->back()->with('success', 'Item(s) removed.');
    }

   
}
