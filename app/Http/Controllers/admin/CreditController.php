<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;

use Illuminate\Database\Migrations\Migration;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB; 
use Illuminate\Http\Request;
use App\Model\Wallet;




class CreditController extends Controller {
    public function index(request $request){
        //

        $list = Wallet::with('user')->find(1);

        $query = Wallet::latest();
        if( !empty( $request->uid ) ) {
            // $query->where('uid', 'LIKE', '%'.$request->uid.'%');
            $query->whereHas('user', function ($q) use ($request) {
                $q->where('name', 'LIKE', '%'.$request->uid.'%')
                  ->orWhere('mobile', 'LIKE', '%'.$request->uid.'%');
            });
        }
        if(!empty($request->type)) {
            $query->where('type', $request->type);
        }
        $lists = $query->paginate(20);

        $data = compact( 'lists','list'); // Variable to array convert
        return view('backend.inc.wallet',$data);
    }
    public function credit(request $request){
        //

        $list = Wallet::with('user')->find(1);

        $query = Wallet::latest()->where('type','credit');
        if( !empty( $request->uid ) ) {
            // $query->where('uid', 'LIKE', '%'.$request->uid.'%');
            $query->whereHas('user', function ($q) use ($request) {
                $q->where('name', 'LIKE', '%'.$request->uid.'%')
                  ->orWhere('mobile', 'LIKE', '%'.$request->uid.'%');
            });
        }
        $lists = $query->paginate(20);

        $data = compact( 'lists','list'); // Variable to array convert
        return view('backend.inc.wallet.credit',$data);
    }
    public function debit(request $request){
        //

        $list = Wallet::with('user')->find(1);

        $query = Wallet::latest()->where('type','debit');
        if( !empty( $request->uid ) ) {
            // $query->where('uid', 'LIKE', '%'.$request->uid.'%');
            $query->whereHas('user', function ($q) use ($request) {
                $q->where('name', 'LIKE', '%'.$request->uid.'%')
                  ->orWhere('mobile', 'LIKE', '%'.$request->uid.'%');
            });
        }
        $lists = $query->paginate(20);

        $data = compact( 'lists','list'); // Variable to array convert
        return view('backend.inc.wallet.debit',$data);
    }
    public function remove(  $id ){
        $remove = Survey::where('id',$id)->delete();
        return redirect( url('admin-control/survey') )->with('success', 'Success! A record has been deleted.');   
    }

    public function removeMultiple(Request $request)
    {
        $check = $request->check; // input type="checkbox" name="check[]"
        Survey::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

        return redirect()->back()->with('success', 'Item(s) removed.');
    }


    public function exportCsv(Request $request) {
        $fileName = 'tasks.csv';
        // $tasks = Wallet::with('user')->where('type','credit')->get()->toArray();


        $tasks = Wallet::with('user')->where('type','credit');
        if( !empty( $request->uid ) ) {
            // $query->where('uid', 'LIKE', '%'.$request->uid.'%');
            $tasks->whereHas('user', function ($q) use ($request) {
                $q->where('name', 'LIKE', '%'.$request->uid.'%')
                  ->orWhere('mobile', 'LIKE', '%'.$request->uid.'%');
            });
        }
        $lists = $tasks->get()->toArray();

        $listArr = [];
        foreach($lists as $key => $task) {
            $arr = $task;
            // $arr = array_merge($task, $task['user']);
            $arr['name']   = $task['user']['name'];
            $arr['mobile']   = $task['user']['mobile'];
            unset($arr['user']);
            unset($arr['id']);
            unset($arr['uid']);
            unset($arr['mobile_no']);
            $listArr[] = $arr;
        }

        $this->download_send_headers("data_export_" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();

        // $headers = array(
        //     "Content-type"        => "text/csv",
        //     "Content-Disposition" => "attachment; filename=$fileName",
        //     "Pragma"              => "no-cache",
        //     "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
        //     "Expires"             => "0"
        // );

        // $columns = array('Title', 'Assign', 'Description', 'Start Date', 'Due Date');

        // $callback = function() use($tasks, $columns) {
        //     $file = fopen('php://output', 'w');
        //     fputcsv($file, $columns);

        //     foreach ($tasks as $task) {
        //         $row['id']  = $task->id;
        //         $row['uid']    = $task->uid->name;
        //         $row['offer']    = $task->offer;
        //         $row['amount']  = $task->amount;
        //         $row['remarks']  = $task->remarks;

        //         fputcsv($file, array($row['id'], $row['uid'], $row['offer'], $row['amount'], $row['remarks']));
        //     }

        //     fclose($file);
        // };

        // return response()->stream($callback, 200, $headers);
    }
          
    public  function array2csv(array &$array)
    {
       if (count($array) == 0) {
         return null;
       }
       ob_start();
       $df = fopen("php://output", 'w');
       fputcsv($df, array_keys(reset($array)));
       foreach ($array as $row) {
          fputcsv($df, $row);
       }
       fclose($df);
       return ob_get_clean();
    }
    public function download_send_headers($filename) {
    // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
        
}