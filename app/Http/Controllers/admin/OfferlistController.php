<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Offer;
 use App\Model\Category;



class OfferlistController extends Controller {
    public function index( Request $request ) {
        // Select Query to get news data
        $query = Offer::where('type', 'offer list')->latest();

        if( !empty( $request->name ) ) {
            $query->where('name', 'LIKE', '%'.$request->name.'%');
        }

        $lists = $query->paginate(20);

        $categories = Category::get();
        $categoryArr = [
            ''  => 'Select Category'
        ];
        foreach($categories as $c) {
            $categoryArr[ $c->name ] = $c->name;
        }

        $data = compact( 'lists','categoryArr' ); // Variable to array convert
        return view('backend.inc.offer-list.index', $data);
    }
    public function add() {

        $categories = Category::get();
        $categoryArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {
            $categoryArr[ $c->name ] = $c->name;
        }

        $data = compact('categoryArr');
        return view('backend.inc.offer-list.add', $data);
       
        // return view('backend.inc.Offer.add');
    }
    public function addData( Request $request ) {
        $rules = [
            'name'        => 'required',
            'category'  => 'required',
            'description'  => 'required',
            'earn_price'  => 'required',
            'playstore_link'  => 'required',
            'apk_size'  => 'required',
            'task'  => 'required',
            'image'  => 'required',
            'type'  => 'required'
        ];
        $request->validate( $rules );
        $file = $request->file('image');
        // $destinationPath = 'public/imgs/shop/';
        // $file->move($destinationPath,$file->getClientOriginalName());

        
            $image       = $request->file('image');
            $filename    = $file->getClientOriginalName();

            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(300, 300);
            $image_resize->save(public_path('imgs/offer-list/' .$filename));

        

        $obj = new Offer;
        $obj->name        = $request->name;
        $obj->category  = $request->category;
        $obj->description  = $request->description;
        $obj->earn_price  = $request->earn_price;
        $obj->playstore_link  = $request->playstore_link;
        $obj->apk_size = $request->apk_size;
        $obj->image  = $file->getClientOriginalName();
        $obj->task  = $request->task;
        $obj->type  = $request->type;
        $obj->tracking_token  = substr(md5(time()), 0, 8);
        $obj->save();


        

        return redirect( url('admin-control/offer-list/') )->with('success', 'Success! New record has been added.');
    }
    public function edit( Request $request, $id ) {

        $edit = Offer::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();


        $categories = Category::get();
        $categoryArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {
            $categoryArr[ $c->name ] = $c->name;
        }

        $data = compact('categoryArr', 'edit');
        // return view('backend.inc.offer-list.edit');
        return view('backend.inc.offer-list.edit', $data);
    }
    public function editData( Request $request, $id ) {
        $rules = [
            'name'        => 'required',
            'category'  => 'required',
            'description'  => 'required',
            'earn_price'  => 'required',
            'playstore_link'  => 'required',
            'apk_size'  => 'required',
            'task'  => 'required',
            // 'image'  => 'required',
            'type'  => 'required'
        ];
        $request->validate( $rules );
        


        $obj = Offer::findOrFail( $id );
         $obj->name        = $request->name;
        $obj->category  = $request->category;
        $obj->description  = $request->description;
        $obj->earn_price  = $request->earn_price;
        $obj->playstore_link  = $request->playstore_link;
        $obj->apk_size = $request->apk_size;
        $obj->task  = $request->task;
        $obj->type  = $request->type;
        $obj->tracking_token  = substr(md5(time()), 0, 8);

        if($request->hasFile('image')){
            $file = $request->file('image');
            // $destinationPath = 'public/imgs/shop/';
            // $file->move($destinationPath,$file->getClientOriginalName());

            $filename    = $file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(300, 300);
            $image_resize->save(public_path('imgs/offer-list/' .$filename));
            $obj->image  = $file->getClientOriginalName();
}
        $obj->save();

        return redirect( url('admin-control/offer-list') )->with('success', 'Success! A record has been updated.');
    }
    public function remove(  $id ){
        $remove = Offer::where('id',$id)->delete();
        return redirect( url('admin-control/offer-list') )->with('success', 'Success! A record has been deleted.');   
    }

    public function removeMultiple(Request $request)
    {
        $check = $request->check; // input type="checkbox" name="check[]"
        Offer::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

        return redirect()->back()->with('success', 'Item(s) removed.');
    }

// SELECT * FROM `news` WHERE `news_id` = 1 or `news_id` = 2
    // News::where('news_id', 1)->orWhere('news_id', 2)->get();
}
