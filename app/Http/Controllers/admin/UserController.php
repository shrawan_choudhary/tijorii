<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

// use Hash;
use Auth;

class UserController extends Controller {
    public function login() {
 
        return view('backend.inc.login');
    }

    public function logout() {
        Auth::logout();

        return redirect( url('admin-control') )->with('success', 'You\'ve logged out');
    }

    public function auth( Request $request ) {

        

        $remember = ($request->has('remember')) ? true : false;



        $rules = [
            'mobile'    => 'required',
            'password'  => 'required|min:6|max:20'
        ];
        $request->validate( $rules );

        $userData = [
            'mobile'    => $request->mobile,
            'password'  => $request->password
        ];
        
        

        

        if( Auth::attempt( $userData, $remember ) ) {
            // Session::flash('message', array('body'=>trans('login-signup.welcome'), 'type'=>'success'));
            return redirect( url('admin-control/dashboard') );
        } else {
            return redirect( url('admin-control') )->with('danger', 'Credentials is not matched.');
        }
    }
    
}
