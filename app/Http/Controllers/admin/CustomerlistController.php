<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;




class CustomerlistController extends Controller {
    public function index(request $request){
        //
        $query = User::where('role', 'user')->latest();

        if (!empty($request->name)) {
        	$query->where('name', 'LIKE', '%'.$request->name.'%');
        }

        $lists = $query->paginate(15);
        $list  = User::where('status', 'Y')->get();
        


        
        // $list = User::with('user')->find(1);
        $data = compact( 'lists','list' ); // Variable to array convert
        return view('backend.inc.customer-list',$data);

    }
    public function change_status(Request $request, User $customer)
    {
        $customer->{$request->field} = $request->status;
        $customer->save();

        return redirect()->back()->with('success', "{$request->field} status has been changed.");
    }

}