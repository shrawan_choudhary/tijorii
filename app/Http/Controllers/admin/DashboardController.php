<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;
use App\Model\Wallet;
use App\Model\Category;
use App\Model\Faq;
use App\Model\Offer;
use App\Model\OfferHistory;
use App\User;

class DashboardController extends Controller {
    public function index() {
    	$credit_wallet = Wallet::where('type','credit')->get();
    	$debit_wallet  = Wallet::where('type', 'debit')->get();
    	$category      = Category::get();
    	$offer         = Offer::where('type', 'offer list')->get();
    	$survey        = Offer::where('type', 'survey')->get();
    	$user          = User::get();
    	$faq           = Faq::get();
    	$credit        = Wallet::where('type', 'credit')->get();
    	$debit        = Wallet::where('type', 'debit')->get();
    	$data=compact('credit_wallet','debit_wallet','category','offer','survey','faq','user','credit','debit');
        return view('backend.inc.dashboard',$data);
    }
}
