<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\OfferHistory;





class OfferhistoriesController extends Controller {
    public function index(request $request){
        //
        $query = OfferHistory::latest();

        
        if( !empty( $request->task_id ) ) {
            // $query->where('uid', 'LIKE', '%'.$request->uid.'%');
            $query->whereHas('offer', function ($q) use ($request) {
                $q->where('name', 'LIKE', '%'.$request->task_id.'%');
            });
        }
        if( !empty($request->status) ){
            $query->where('status','LIKE', '%'.$request->status.'%');
        }

        $list = $query->with('users')->whereHas('offer', function ($q) {
            $q;})->paginate();
        

        // $specialMenues  = MenuItem::with(['media','category'])->where('status', 'Y')->where('is_available', 'Y')->get();
        // $list = OfferHistory::with('user')->find(1);
        // $lists = $query->paginate(15);
        $data = compact( 'list' ); // Variable to array convert


        return view('backend.inc.offer-histories',$data);

    }


    

    public function remove(  $id ){
        $remove = OfferHistory::where('id',$id)->delete();
        return redirect( url('admin-control/offer-histories') )->with('success', 'Success! A record has been deleted.');   
    }

    public function removeMultiple(Request $request)
    {
        $check = $request->check; // input type="checkbox" name="check[]"
        OfferHistory::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

        return redirect()->back()->with('success', 'Item(s) removed.');
    }
    public function change_status(Request $request, OfferHistory $id)
    {
        $id->update([ $request->field => $request->status ]);
        

        return redirect()->back()->with('success', "{$request->field} status has been changed.");
    }

    public function exportCsv(Request $request) {
        $fileName = 'tasks.csv';
        // $tasks = OfferHistory::with('users','offer')->get()->toArray();
        $tasks = OfferHistory::with('users','offer')->whereHas('offer', function ($q) {
            $q->where('type', 'offer-list');});
        if( !empty( $request->task_id ) ) {
            // $query->where('uid', 'LIKE', '%'.$request->uid.'%');
            $tasks->whereHas('offer', function ($q) use ($request) {
                $q->where('name', 'LIKE', '%'.$request->task_id.'%');
            });
        }
        if( !empty($request->status) ){
            $tasks->where('status','LIKE', '%'.$request->status.'%');
        }
        $lists = $tasks->get()->toArray();

        $listArr = [];
        foreach($lists as $key => $task) {
            $arr = $task;
            // $arr = array_merge($task, $task['user']);
            $arr['name']   = $task['users']['name'];
            $arr['name']   = $task['offer']['name'];
            unset($arr['users']);
            unset($arr['task_id']);
            unset($arr['updated_at']);
            // unset($arr['mobile_no']);
            unset($arr['offer']);
            $listArr[] = $arr;
        }

        // echo "<pre>"; print_r($listArr); die;

        $this->download_send_headers("data_export_" . date("Y-m-d") . ".csv");
        echo $this->array2csv($listArr);
        die();


        // $headers = array(
        //     "Content-type"        => "text/csv",
        //     "Content-Disposition" => "attachment; filename=$fileName",
        //     "Pragma"              => "no-cache",
        //     "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
        //     "Expires"             => "0"
        // );

        // $columns = array('Title', 'Assign', 'Description', 'Start Date', 'Due Date');

        // $callback = function() use($tasks, $columns) {
        //     $file = fopen('php://output', 'w');
        //     fputcsv($file, $columns);

        //     foreach ($tasks as $task) {
        //         $row['id']  = $task->id;
        //         $row['uid']    = $task->uid->name;
        //         $row['offer']    = $task->offer;
        //         $row['amount']  = $task->amount;
        //         $row['remarks']  = $task->remarks;

        //         fputcsv($file, array($row['id'], $row['uid'], $row['offer'], $row['amount'], $row['remarks']));
        //     }

        //     fclose($file);
        // };

        // return response()->stream($callback, 200, $headers);
    }
          
    public  function array2csv(array &$array)
    {
       if (count($array) == 0) {
         return null;
       }
       ob_start();
       $df = fopen("php://output", 'w');
       fputcsv($df, array_keys(reset($array)));
       foreach ($array as $row) {
          fputcsv($df, $row);
       }
       fclose($df);
       return ob_get_clean();
    }
    public function download_send_headers($filename) {
    // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download  
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }


}