<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class TablesController extends Controller {
    public function index() {
        return view('backend.inc.tables');
    }
}
