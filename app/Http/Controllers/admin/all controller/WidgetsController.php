<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class WidgetsController extends Controller {
    public function index() {
        return view('backend.inc.widgets');
    }
}
