<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class GridController extends Controller {
    public function index() {
        return view('backend.inc.grid');
    }
}
