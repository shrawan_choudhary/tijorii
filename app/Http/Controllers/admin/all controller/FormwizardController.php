<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class FormwizardController extends Controller {
    public function index() {
        return view('backend.inc.form-wizard');
    }
}
