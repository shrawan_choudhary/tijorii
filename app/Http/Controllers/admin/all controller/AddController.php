<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class AddController extends Controller {
    public function index() {
        return view('backend.inc.offer-list/add');
    }
}
