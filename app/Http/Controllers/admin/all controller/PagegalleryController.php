<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class PagegalleryController extends Controller {
    public function index() {
        return view('backend.inc.pages-gallery');
    }
}
