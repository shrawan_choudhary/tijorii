<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class BasicformController extends Controller {
    public function index() {
        return view('backend.inc.form-basic');
    }
}
