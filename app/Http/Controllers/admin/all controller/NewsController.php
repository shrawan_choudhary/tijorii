<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

use App\Model\News;

class NewsController extends Controller {
    public function index() {
        // Select Query to get news data
        $lists = News::latest()->paginate(2);

        $data = compact( 'lists' ); // Variable to array convert
        return view('backend.inc.news.index', $data);
    }
    public function add() {
       
        return view('backend.inc.news.add');
    }
    public function addData( Request $request ) {
        $rules = [
            'news_title'        => 'required',
            'news_description'  => 'required'
        ];
        $request->validate( $rules );

        $obj = new News;
        $obj->news_title        = $request->news_title;
        $obj->news_description  = $request->news_description;
        $obj->save();

        return redirect( url('admin-control/news/index') )->with('success', 'Success! New record has been added.');
    }
    public function edit( Request $request, $id ) {

        $edit = News::findOrFail( $id )->toArray();
        $request->replace($edit);
        $request->flash();

        return view('backend.inc.news.edit');
    }
    public function editData( Request $request, $id ) {
        $rules = [
            'news_title'        => 'required',
            'news_description'  => 'required'
        ];
        $request->validate( $rules );

        $obj = News::findOrFail( $id );
        $obj->news_title        = $request->news_title;
        $obj->news_description  = $request->news_description;
        $obj->save();

        return redirect( url('admin-control/news') )->with('success', 'Success! A record has been updated.');
    }
    public function remove(  $id ){
        $remove = News::where('id',$id)->delete();
        return redirect( url('admin-control/news') )->with('success', 'Success! A record has been deleted.');   
    }
}
