<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class PageelementController extends Controller {
    public function index() {
        return view('backend.inc.page-elements');
    }
}
