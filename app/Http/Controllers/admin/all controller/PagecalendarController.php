<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class PagecalendarController extends Controller {
    public function index() {
        return view('backend.inc.page-calendar');
    }
}
