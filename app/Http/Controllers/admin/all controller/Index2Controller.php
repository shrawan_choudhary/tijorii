<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class Index2Controller extends Controller {
    public function index() {
        return view('backend.inc.index2');
    }
}
