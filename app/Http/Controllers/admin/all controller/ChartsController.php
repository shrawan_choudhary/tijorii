<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class ChartsController extends Controller {
    public function index() {
        return view('backend.inc.charts');
    }
}
