<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class IconmaterialController extends Controller {
    public function index() {
        return view('backend.inc.icon-material');
    }
}
