<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class IconfontawesomeController extends Controller {
    public function index() {
        return view('backend.inc.icon-fontawesome');
    }
}
