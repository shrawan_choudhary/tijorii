<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller;

class Error500Controller extends Controller {
    public function index() {
        return view('backend.inc.error-500');
    }
}
