<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Setting;




class SettingController extends Controller {
    public function index(request $request) {

        $lists = Setting::latest()->paginate();
        

        $data = compact( 'lists' ); // Variable to array convert
        return view('backend.inc.setting.index', $data);
    }


   
    public function edit( Request $request, $id ) {

        $edit = Setting::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();

        return view('backend.inc.setting.edit', compact('edit'));

    }
    public function editData( Request $request, $id ) {
        $rules = [
            'app_name'        => 'required',
            'app_version'  => 'required',
            'mobile_no'  => 'required',
            'email'  => 'required',
            'address'  => 'required',
            'refer_prefix'  => 'required',
            'refer_point'  => 'required',
            'game_link'  => 'required',
            'about'  => 'required',
            'banner_title'  => 'required',
            'banner_desc'  => 'required',
        ];
        $request->validate( $rules );
        

        $obj = Setting::findOrFail( $id );
         $obj->app_name        = $request->app_name;
        $obj->app_version  = $request->app_version;
        $obj->mobile_no  = $request->mobile_no;
        $obj->email  = $request->email;
        $obj->address  = $request->address;
        $obj->refer_prefix = $request->refer_prefix;
        $obj->refer_point  = $request->refer_point;
        $obj->game_link  = $request->game_link;
        $obj->about  = $request->about;
        $obj->banner_title  = $request->banner_title;
        $obj->banner_desc  = $request->banner_desc;
        $obj->twitter  = $request->twitter;
        $obj->instagram  = $request->instagram;
        $obj->facebook  = $request->facebook;
        $obj->linkedin  = $request->linkedin;
        $obj->contact_page_desc  = $request->contact_page_desc;

        if($request->hasFile('banner_image'))  { 
            
            $image       = $request->file('banner_image');
            $filename    = $image->getClientOriginalName('banner_image');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(1920, 1173);
            $image_resize->save(public_path('imgs/' .$filename));
            $obj->banner_image   = $image->getClientOriginalName();
        }
        if($request->hasFile('logo'))  { 
            
            $image       = $request->file('logo');
            $filename    = $image->getClientOriginalName('logo');
            $image_resize = Image::make($image->getRealPath());              
            // $image_resize->resize(750, 500);
            $image_resize->save(public_path('imgs/' .$filename));
            $obj->logo   = $image->getClientOriginalName();
        }
        if($request->hasFile('favicon'))  { 
            
            $image       = $request->file('favicon');
            $filename    = $image->getClientOriginalName('favicon');
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(50, 50);
            $image_resize->save(public_path('imgs/' .$filename));
            $obj->favicon   = $image->getClientOriginalName();
        }

        $obj->save();

        return redirect()->back()->with('success', 'Success! A record has been updated.');
    }
    

// SELECT * FROM `news` WHERE `news_id` = 1 or `news_id` = 2
    // News::where('news_id', 1)->orWhere('news_id', 2)->get();
}


   
    
    
   
   

