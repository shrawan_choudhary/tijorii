<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Page;

class PagesController extends Controller
{
    public function index()
    {
        $lists 	= Page::latest()->paginate(15);
        $count 	= Page::count();
        $data 	= compact('lists'); // Variable to array convert
        return view('backend.inc.pages.index', $data)->with('count', $count);
    }
    public function edit(Request $request, $id)
    {
        $edit = Page::findOrFail($id);
        $request->replace($edit->toArray());
        $request->flash();
        return view('backend.inc.pages.edit', compact('edit'));
    }
    public function editData(Request $request, $id)
    {
        $rules = [
            'description' => 'required'
        ];
        $request->validate($rules);
        $obj 			  = Page::findOrFail($id);
        $obj->description = $request->description;
        $obj->save();
        return redirect(url('admin-control/pages'))->with('success', 'Success! A record has been updated.');
    }
}
