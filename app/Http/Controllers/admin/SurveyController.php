<?php

namespace App\Http\Controllers\admin;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Survey;
 use App\Model\Category;



class SurveyController extends Controller {
    public function index(request $request) {
        
        $query = Survey::where('type', 'survey')->latest();

        if( !empty( $request->name ) ) {
            $query->where('name', 'LIKE', '%'.$request->name.'%');
        }


        $lists = $query->paginate(20);

        $categories = Category::get();
        $categoryArr = [
            ''  => 'Select Category'
        ];
        foreach($categories as $c) {
            $categoryArr[ $c->name ] = $c->name;
        }
        

        // $query = request()->get('name');

        $producten = Survey::where('name', 'LIKE',  "%$request->name%")->get();
        
         

        $data = compact( 'lists','categoryArr' ); // Variable to array convert
        // return view('backend.inc.survey.index', $data);


       return view('backend.inc.survey.index',$data);
    }
    
   
    public function add() {

        $categories = Category::get();
        $categoryArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {
            $categoryArr[ $c->name ] = $c->name;
        }
        

        $data = compact('categoryArr');
        return view('backend.inc.survey.add', $data);
       
        // return view('backend.inc.survey.add');
    }
    public function addData( Request $request ) {
        $rules = [
            'name'        => 'required',
            'category'  => 'required',
            'description'  => 'required',
            'earn_price'  => 'required',
            'playstore_link'  => 'required',
            'apk_size'  => 'required',
            'task'  => 'required',
            'image'  => 'required',
            'type'  => 'required'

        ];
        $request->validate( $rules );
        // $token = substr(md5(time()), 0, 8);
        $file = $request->file('image');
        // $destinationPath = 'public/imgs/shop/';
        // $file->move($destinationPath,$file->getClientOriginalName());

        
            $image       = $request->file('image');
            $filename    = $file->getClientOriginalName();

            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(300, 300);
            $image_resize->save(public_path('imgs/survey/' .$filename));

        

        $obj = new Survey;
        $obj->name        = $request->name;
        $obj->category  = $request->category;
        $obj->description  = $request->description;
        $obj->earn_price  = $request->earn_price;
        $obj->playstore_link  = $request->playstore_link;
        $obj->apk_size = $request->apk_size;
        $obj->image  = $file->getClientOriginalName();
        $obj->task  = $request->task;
        $obj->type  = $request->type;
        $obj->tracking_token  = substr(md5(time()), 0, 8);
        $obj->save();


        

        return redirect( url('admin-control/survey/') )->with('success', 'Success! New record has been added.');
    }
    public function edit( Request $request, $id ) {

        $edit = Survey::findOrFail( $id );
        $request->replace($edit->toArray());
        $request->flash();

        // return view('backend.inc.survey.edit');
        $categories = Category::get();
        $categoryArr = [
            ''  => 'Select Category'
        ];

        foreach($categories as $c) {
            $categoryArr[ $c->name ] = $c->name;
        }
        

        $data = compact('categoryArr','edit');

        // return view('backend.inc.offer-list.edit');
        return view('backend.inc.survey.edit', $data);
    }
    public function editData( Request $request, $id ) {
        $rules = [
            'name'        => 'required',
            'category'  => 'required',
            'description'  => 'required',
            'earn_price'  => 'required',
            'playstore_link'  => 'required',
            'apk_size'  => 'required',
            'task'  => 'required',
            // 'image'  => 'required',
            'type'  => 'required'
        ];
        $request->validate( $rules );

        $obj = Survey::findOrFail( $id );
         $obj->name        = $request->name;
        $obj->category  = $request->category;
        $obj->description  = $request->description;
        $obj->earn_price  = $request->earn_price;
        $obj->playstore_link  = $request->playstore_link;
        $obj->apk_size = $request->apk_size;
        $obj->task  = $request->task;
        $obj->type  = $request->type;

         if($request->hasFile('image'))  { 
            $file = $request->file('image');
            // $destinationPath = 'public/imgs/shop/';
            // $file->move($destinationPath,$file->getClientOriginalName());
            $filename    = $file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(300, 300);
            $image_resize->save(public_path('imgs/survey/' .$filename));
            $obj->image  = $file->getClientOriginalName();
}
        $obj->save();

        return redirect( url('admin-control/survey') )->with('success', 'Success! A record has been updated.');
    }
    public function remove(  $id ){
        $remove = Survey::where('id',$id)->delete();
        return redirect( url('admin-control/survey') )->with('success', 'Success! A record has been deleted.');   
    }

    public function removeMultiple(Request $request)
    {
        $check = $request->check; // input type="checkbox" name="check[]"
        Survey::whereIn("id", $check)->delete(); // DELETE FROM news WHERE news_id IN (3,5,4)

        return redirect()->back()->with('success', 'Item(s) removed.');
    }

// SELECT * FROM `news` WHERE `news_id` = 1 or `news_id` = 2
    // News::where('news_id', 1)->orWhere('news_id', 2)->get();
}
