<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'api'], function () {
    Route::middleware('guest')->post('send-otp', 'UserController@sendOtp');
    Route::middleware('guest')->post('login', 'UserController@login');
    Route::middleware('guest')->post('register', 'UserController@register');

    Route::post('page-info', 'PageController@index');
    Route::get('faqs', 'FaqController@index');

    Route::get('task/complete/{id}', 'OfferHistoryController@change_status');

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('home', 'HomeController@index');
        Route::get('daily-check-in', 'DailyPointsController@earn_point');
        Route::post('profile', 'UserController@details');

        Route::group(['prefix' => 'wallet'], function () {
            Route::get('/', 'WalletController@index');
            Route::post('withdraw', 'WalletController@withdraw');
        });

        Route::group(['prefix' => 'offer'], function () {
            Route::get('/', 'OfferController@index')->name('offer');
            Route::get('/history', 'OfferController@history')->name('offer_history');
            Route::get('/{offer}', 'OfferController@info');
        });

        Route::group(['prefix' => 'survey'], function () {
            Route::get('/', 'OfferController@index')->name('survey');
            Route::get('/history', 'OfferController@history')->name('survey_history');
            Route::get('/{offer}', 'OfferController@info');
        });

        Route::group(['prefix' => 'task'], function () {
            Route::post('/save', 'OfferHistoryController@save');
        });
    });
});
