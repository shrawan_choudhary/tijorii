<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index'); 
Route::get('/contact', 'HomeController@contact');
Route::post('ajax_request', 'HomeController@enquiry')->name('ajax-route');
Route::get('/{slug}', 'HomeController@pages'); 
// Route::get('/charts', 'ChartsController@index'); 
Route::get('/spin-wheel/{user:referal_code}', 'SpinWheelController@index');
Route::post('/spin-wheel/save', 'SpinWheelController@save'); 
// Route::get('/charts', 'ChartsController@index'); 



    
