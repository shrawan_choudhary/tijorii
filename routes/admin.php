<?php


use Illuminate\Support\Facades\Route;

// Login
    Route::get('admin-control/mobile', 'admin\MobileController@index');
    Route::get('admin-control/reset-password', 'admin\ResetpasswordController@index');

Route::group(['prefix' => 'admin-control', 'middleware' => 'guest'], function() {
    Route::get( '/', [ 'as' => 'login', 'uses' => 'admin\UserController@login'] );
    Route::post( 'login-auth', 'admin\UserController@auth' );
});

// Logout

Route::group(['prefix' => 'admin-control', 'middleware' => 'auth'], function() {
    Route::get( 'logout', 'admin\UserController@logout' );
    Route::get( 'dashboard', ['as' => 'home', 'uses' => 'admin\DashboardController@index' ]  );

// dashboard

    Route::get('dashboard','admin\DashboardController@index');
    

// survey

    Route::get('survey', 'admin\SurveyController@index');    
    Route::get('survey/add','admin\SurveyController@add');
    Route::post('survey/add','admin\SurveyController@adddata');
    Route::get('survey/edit/{id}', 'admin\SurveyController@edit');
    Route::post('survey/edit/{id}', 'admin\SurveyController@editdata');
    Route::get('survey/remove/{id}','admin\SurveyController@remove');
    Route::post('survey/removeMultiple','admin\SurveyController@removeMultiple');    


// offer-list

    Route::get('offer-list', 'admin\OfferlistController@index');
    Route::get('offer-list/add','admin\OfferlistController@add');
    Route::post('offer-list/add','admin\OfferlistController@adddata');
    Route::get('offer-list/edit/{id}', 'admin\OfferlistController@edit');
    Route::post('offer-list/edit/{id}', 'admin\OfferlistController@editdata');
    Route::get('offer-list/remove/{id}','admin\OfferlistController@remove');
    Route::post('offer-list/removeMultiple','admin\OfferlistController@removeMultiple');

// category

    Route::get('category', 'admin\CategoryController@index');
    Route::get('category/add', 'admin\CategoryController@add');
    Route::post('category/add', 'admin\CategoryController@adddata');
    Route::get('category/edit/{id}', 'admin\CategoryController@edit');
    Route::post('category/edit/{id}', 'admin\CategoryController@editdata');
    Route::get('category/remove/{id}','admin\CategoryController@remove');
    Route::post('category/removeMultiple','admin\CategoryController@removeMultiple');
    
// Faqs

    Route::get('faqs', 'admin\FaqsController@index');
    Route::get('faqs/add','admin\FaqsController@add');
    Route::post('faqs/add','admin\FaqsController@adddata');
    Route::get('faqs/edit/{id}', 'admin\FaqsController@edit');
    Route::post('faqs/edit/{id}', 'admin\FaqsController@editdata');
    Route::get('faqs/remove/{id}','admin\FaqsController@remove');
    Route::post('faqs/removeMultiple','admin\FaqsController@removeMultiple');

// pages

    Route::get('pages', 'admin\PagesController@index');
    Route::get('pages/edit/{id}', 'admin\PagesController@edit');
    Route::post('pages/edit/{id}', 'admin\PagesController@editdata');
    Route::post('pages/removeMultiple','admin\PagesController@removeMultiple');

// Setting

    Route::get('setting', 'admin\SettingController@index');
    Route::get('setting/edit/{id}', 'admin\SettingController@edit');
    Route::post('setting/edit/{id}', 'admin\SettingController@editdata');

    

// change password

    Route::get('/change-password', 'admin\ChangepasswordController@index');
    Route::post('/change-password','admin\ChangepasswordController@changePassword');


// Wallet

    Route::get('/wallet/credit', 'admin\CreditController@credit');
    Route::get('/wallet/debit', 'admin\DebitController@debit');
    Route::get('/wallet/remove/{id}', 'admin\WalletController@remove');
    Route::get('/wallet/removeMultiple', 'admin\WalletController@removeMultiple');

    // Route::get('/wallet', 'admin\WalletController@excel')->name('export_excel.excel');
    Route::get('/wallet/credit/export', 'admin\CreditController@exportCsv')->name('credit_export_wallet');
    Route::get('/wallet/debit/export', 'admin\DebitController@exportCsv')->name('debit_export_wallet');
    Route::get('/wallet/debit/status/{id}', 'admin\DebitController@change_status')->name('change_status_wallet');

// Offer Histories

    Route::get('/offer-histories','admin\OfferhistoriesController@index'); 
    Route::get('/offer-histories/remove/{id}', 'admin\OfferhistoriesController@remove');
    Route::get('/offer-histories/removeMultiple', 'admin\OfferhistoriesController@removeMultiple');
     Route::get('/offer-histories/status/{id}', 'admin\OfferhistoriesController@change_status')->name('change_status');

// Offer Histories

    Route::get('/offer-histories','admin\OfferhistoriesController@index'); 
    Route::get('/offer-histories/remove/{id}', 'admin\OfferhistoriesController@remove');
    Route::get('/offer-histories/removeMultiple', 'admin\OfferhistoriesController@removeMultiple');
     Route::get('/offer-histories/status/{id}', 'admin\OfferhistoriesController@change_status')->name('change_status');
      Route::get('/offer-histories/export', 'admin\OfferhistoriesController@exportCsv')->name('offer_export');

     

// Survey Histories

    Route::get('/survey-histories', 'admin\SurveyhistoriesController@index');
    Route::get('/survey-histories/remove/{id}', 'admin\SurveyhistoriesController@remove');
    Route::get('/survey-histories/removeMultiple', 'admin\SurveyhistoriesController@removeMultiple');
    Route::get('/survey-histories/export', 'admin\SurveyhistoriesController@exportCsv')->name('survey_export');
    
    

// Customer List
    Route::get('/customer-list', 'admin\CustomerlistController@index');
    Route::get('/customer-list/remove/{id}', 'admin\CustomerlistController@remove');
    Route::get('/customer-list/removeMultiple', 'admin\CustomerlistController@removeMultiple');
    Route::get('/customer-list/status/{customer}', 'admin\CustomerlistController@change_status')->name('change_customer_status');

// Forgot Password
// Password Reset Routes...
    Route::get('/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset.token');
    Route::post('/reset', 'ResetPasswordController@reset');
    
// Spin Wheels
    Route::get('/spin', 'admin\SpinController@index');
    Route::get('spin/edit/{id}', 'admin\SpinController@edit');
    Route::post('spin/edit/{id}', 'admin\SpinController@editdata');

// Paytm

    Route::get('/initiate','admin\PaytmController@initiate')->name('initiate.payment');
    Route::post('/payment','admin\PaytmController@pay')->name('make.payment');
    Route::post('/payment/status', 'admin\PaytmController@paymentCallback')->name('status');
    Route::get('/initiate/{id}', 'admin\DebitController@payNow');
    
// All Pages Controllers

    Route::get('/charts','admin\ChartsController@index');
    Route::get('/widgets','admin\WidgetsController@index');
    Route::get('/tables','admin\TablesController@index');
    Route::get('/grid','admin\GridController@index');
    Route::get('/form-basic','admin\BasicformController@index');
    Route::get('/form-wizard','admin\FormwizardController@index');
    Route::get('/page-button','admin\PagebuttonController@index');
    Route::get('/icon-material','admin\IconmaterialController@index');
    Route::get('/icon-fontawesome','admin\IconfontawesomeController@index');
    Route::get('/page-element','admin\PageelementController@index');
    Route::get('/index2','admin\Index2Controller@index');
    Route::get('/pages-gallery','admin\PagegalleryController@index');
    Route::get('/page-calendar','admin\PagecalendarController@index');
    Route::get('/page-invoice','admin\PageinvoiceController@index');
    Route::get('/page-chat','admin\PagechatController@index');
    Route::get('/authentication-login','admin\AuthenticationloginController@index');
    Route::get('/authentication-register','admin\AuthenticationregisterController@index');
    Route::get('/error-403','admin\Error403Controller@index');
    Route::get('/error-404','admin\Error404Controller@index');
    Route::get('/error-405','admin\Error405Controller@index');
    Route::get('/error-500','admin\Error500Controller@index');

    // Route::post('news/add','admin\NewsController@adddata');
    // Route::get('news/edit/{id}', 'admin\NewsController@edit');
    // Route::post('news/edit/{id}', 'admin\NewsController@editdata');

    // Route::get('news/remove/{id}','admin\NewsController@remove');
});


    
