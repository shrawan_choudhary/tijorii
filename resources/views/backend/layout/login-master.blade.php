<!DOCTYPE html>
<html>
<head>
	<link rel="icon" type="image/png" sizes="16x16" href="{{url('admin/imgs/favicon.png')}}">
	<title>Tijorii-@yield('title')</title>
	{{Html::style('admin/css/style.css')}}
	{{Html::style('css/bootstrap.min.css')}}

</head>
<body style="background: #a9a9a9;">

	<div class="container">
		@yield('contant')
	</div>
</body>
</html>