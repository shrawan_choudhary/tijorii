<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('admin/imgs/favicon.png')}}">
    <title>Tijorii-@yield('title')</title>
    <!-- Custom CSS -->
    <!-- <link href="../../assets/libs/flot/css/float-chart.css" rel="stylesheet"> -->
    {{Html::style('admin/css/float-chart.css')}}
    <!-- {{Html::style('admin/css/bootstrap.min.css')}} -->
    <!-- {{Html::style('admin/css/jquery-ui.css')}} -->
    <!-- {{Html::style('admin/css/materialdesignicons.min.css')}} -->
    {{Html::style('admin/css/style.min.css')}}
    {{Html::style('admin/icomoon/style.css')}}


    <!-- Custom CSS -->
    <!-- <link href="../../dist/css/style.min.css" rel="stylesheet"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
    @section('header')
    <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" style="font-size: 40px;" href="{{url('admin-control/dashboard')}}">
                        <!-- Logo icon -->
                        <b class="logo-icon p-l-10" style="color: orange;">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                             
                            <img src="{{url('admin/imgs/logo-icon.png')}}" alt="homepage" class="light-logo" />
                           
                        </b>
                        <!--End Logo icon -->
                         <!-- Logo text -->
                        <span class="logo-text" style="font-family: sans;">
                             <!-- dark Logo text -->
                             <!-- <img src="{{url('admin/imgs/logo-text.png')}}" alt="homepage" class="light-logo" /> -->
                            TIJORII
                        </span>
                        <!-- Logo icon -->
                        <!-- <b class="logo-icon"> -->
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <!-- <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" /> -->
                            
                        <!-- </b> -->
                        <!--End Logo icon -->
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- create new -->
                        <!-- ============================================================== -->
                       
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{url('admin/imgs/users/1.jpg')}}" alt="user" class="rounded-circle" width="31"></a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                <!-- <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet m-r-5 m-l-5"></i> My Balance</a> -->
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{url('admin-control/change-password')}}"><i class="ti-email m-r-5 m-l-5"></i> Change Password</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{url('admin-control/setting/edit/1')}}"><i class="ti-settings m-r-5 m-l-5"></i> Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{url('admin-control/logout')}}"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                                
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
    @show


   	
    		<aside class="left-sidebar"  data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin-control/dashboard')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
                        
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-file-tree"></i><span class="hide-menu">Category </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="{{url('admin-control/category/add')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Add </span></a></li>
                                <li class="sidebar-item"><a href="{{url('admin-control/category/')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> View </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Offer List </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="{{url('admin-control/offer-list/add')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Add </span></a></li>
                                <li class="sidebar-item"><a href="{{url('admin-control/offer-list/')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> View </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-clipboard-text"></i><span class="hide-menu">Survey </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="{{url('admin-control/survey/add')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Add </span></a></li>
                                <li class="sidebar-item"><a href="{{url('admin-control/survey/')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> View </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin-control/pages/')}}" aria-expanded="false"><i class="mdi mdi-google-pages"></i><span class="hide-menu">Pages</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-comment-question-outline"></i><span class="hide-menu">FAQs </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="{{url('admin-control/faqs/add')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Add </span></a></li>
                                <li class="sidebar-item"><a href="{{url('admin-control/faqs/')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> View </span></a></li>
                            </ul>
                        </li>

                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-format-list-bulleted"></i><span class="hide-menu">Tasks </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="{{url('admin-control/offer-histories')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Offer Task </span></a></li>
                                <li class="sidebar-item"><a href="{{url('admin-control/survey-histories')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> Survey Task </span></a></li>
                            </ul>
                        </li>
                         <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-wallet"></i><span class="hide-menu">Wallet </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="{{url('admin-control/wallet/credit')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Credit </span></a></li>
                                <li class="sidebar-item"><a href="{{url('admin-control/wallet/debit')}}" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> Debit </span></a></li>
                            </ul>
                        </li>
                         <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin-control/customer-list/')}}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Customer-List</span></a></li>
                         <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin-control/spin/edit/1')}}" aria-expanded="false"><i class="mdi mdi-reload"></i><span class="hide-menu">Spin Wheels</span></a></li>
                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
    	
    		
    		@yield('contant')    

    	
    
   

	<div class="container">
		
	</div>

	@section('footer')
		
	@show

{{ Html::script('admin/js/validation.js') }}
{{ Html::script('admin/js/jquery.min.js') }}
{{ Html::script('admin/js/popper.min.js') }}
{{ Html::script('admin/js/bootstrap.min.js') }}
{{ Html::script('admin/js/perfect-scrollbar.jquery.min.js') }}
<!-- {{ Html::script('admin/js/perfect-scrollbar.jquery.min.js') }} -->
{{ Html::script('admin/js/sparkline.js') }}
{{ Html::script('admin/js/waves.js') }}
{{ Html::script('admin/js/sidebarmenu.js') }}
{{ Html::script('admin/js/custom.min.js') }}
<!-- {{ Html::script('admin/js/waves.js') }} -->
{{ Html::script('admin/js/excanvas.js') }}
{{ Html::script('admin/js/jquery.flot.js') }}
{{ Html::script('admin/js/jquery.flot.pie.js') }}
{{ Html::script('admin/js/jquery.flot.time.js') }}
{{ Html::script('admin/js/jquery.flot.stack.js') }}
{{ Html::script('admin/js/jquery.flot.crosshair.js') }}
{{ Html::script('admin/js/jquery.flot.tooltip.min.js') }}
{{ Html::script('admin/js/chart-page-init.js') }}
{{ Html::script('admin/js/voucher-codes.js') }}


	
    
</body>
</html>