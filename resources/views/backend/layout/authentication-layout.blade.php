<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('admin/imgs/favicon.png')}}">
    <title>Matrix-@yield('title')</title>
    <!-- Custom CSS -->
    <!-- <link href="../../assets/libs/flot/css/float-chart.css" rel="stylesheet"> -->
    {{Html::style('admin/css/float-chart.css')}}
    <!-- {{Html::style('admin/css/bootstrap.min.css')}} -->
    <!-- {{Html::style('admin/css/jquery-ui.css')}} -->
    <!-- {{Html::style('admin/css/materialdesignicons.min.css')}} -->
    {{Html::style('admin/css/style.min.css')}}


    <!-- Custom CSS -->
    <!-- <link href="../../dist/css/style.min.css" rel="stylesheet"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>

@section('header')

@show

@yield('contant')


@section('footer')

@show


{{ Html::script('admin/js/jquery.min.js') }}
{{ Html::script('admin/js/popper.min.js') }}
{{ Html::script('admin/js/perfect-scrollbar.jquery.min.js') }}
{{ Html::script('admin/js/bootstrap.min.js') }}
{{ Html::script('admin/js/perfect-scrollbar.jquery.min.js') }}
{{ Html::script('admin/js/sparkline.js') }}
{{ Html::script('admin/js/waves.js') }}
{{ Html::script('admin/js/sidebarmenu.js') }}
{{ Html::script('admin/js/custom.min.js') }}
{{ Html::script('admin/js/waves.js') }}
{{ Html::script('admin/js/excanvas.js') }}
{{ Html::script('admin/js/jquery.flot.js') }}
{{ Html::script('admin/js/jquery.flot.pie.js') }}
{{ Html::script('admin/js/jquery.flot.time.js') }}
{{ Html::script('admin/js/jquery.flot.stack.js') }}
{{ Html::script('admin/js/jquery.flot.crosshair.js') }}
{{ Html::script('admin/js/jquery.flot.tooltip.min.js') }}
{{ Html::script('admin/js/chart-page-init.js') }}
    
    
</body>
</html>