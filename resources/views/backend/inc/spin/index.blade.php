@extends('backend.layout.master')

@section('title', 'spin')

@section('contant')
<div class="page-wrapper p-5">
<hr>
<div class="">
    @if (\Session::has('success'))
        <div class="alert alert-success" style="color: green">
            {!! \Session::get('success') !!}</li>
        </div>
    @endif

    @if (\Session::has('danger'))
        <div class="alert alert-danger" style="color: red;">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif

    @if( !$lists->isEmpty() )
    <div class="table-responsive" style="background: #fff;">
        

        <!-- {{ Form::open( ['url' => url('admin-control/survey/removeMultiple'), 'method'=>'post'] ) }} -->
        <table border="1" style="width: 100%" class="table table-bordered">
            <thead style="font-size: 16px; color: #000;">
                <tr>
                    <th>S.no</th>
                    <th>number1</th>
                    <th>number2</th>
                    <th>number3</th>
                    <th>number4</th>
                    <th>number5</th>
                    <th>number6</th>
                    <th>number7</th>
                    <th>number8</th>
                    <th>number9</th>
                    <th>number10</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                $sn = $lists->firstItem();
                @endphp
                @foreach( $lists as $list )
                <tr>
                    <td>{{ $sn++ }}.</td>
                    <td>{{ $list->number1 }} </td>
                    <td>{{ $list->number2 }}</td>
                    <td>{{ $list->number3 }}</td>
                    <td>{{ $list->number4 }}</td>
                    <td>{{ $list->number5 }}</td>
                    <td>{{ $list->number6 }}</td>
                    <td>{{ $list->number7 }}</td>
                    <td>{{ $list->number8 }}</td>
                    <td>{{ $list->number9 }}</td>
                    <td>{{ $list->number10 }}</td>
                    <td><a href="{{ url('admin-control/spin/edit/'.$list->id) }}">EDIT</a>&nbsp;
                    
                </tr>
                @endforeach
            </tbody>
        </table>
        <!-- <button type="submit" >Selected Delete</button> -->
    {{ Form::close() }}
    </div>

    {{ $lists->links() }}
    @endif
</div>
<hr>
</div>
@stop
