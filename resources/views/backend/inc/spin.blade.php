@extends('backend.layout.master')

@section('title', 'Spin-Wheels')

@section('contant')
<div class="page-wrapper p-5">
    
    <h1 style="text-align: center;">Edit Wheels</h1>
    <div class="">
        @if( $errors->any() )
            <div class="alert alert-danger" style="color: red;">
                @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </div>
        @endif

        @if (\Session::has('success'))
            <div class="alert alert-success" style="color: green">
                {!! \Session::get('success') !!}</li>
            </div>
        @endif

        @if (\Session::has('danger'))
            <div class="alert alert-danger" style="color: red;">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif

        {{ Form::open(['files' => true])  }}
        <p class="">
            {{ Form::label('number1') }}
            {{ Form::text('number1', '', ['class' => 'form-control']) }}
        </p>
        <p>
            {{ Form::label('number2') }}
            {{ Form::text('number2', '', ['class' => 'form-control']) }}

        </p>                                
        <p class="">
            {{ Form::label('number3') }}
            {{ Form::text('number3', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('number4') }}
            {{ Form::text('number4', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('number5') }}
            {{ Form::text('number5', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('number6') }}
            {{ Form::text('number6', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('number7') }}
            {{ Form::text('number7', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('number8') }}
            {{ Form::text('number8', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('number9') }}
            {{ Form::text('number9', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('number10') }}
            {{ Form::text('number10', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::submit('Save', ['class' => 'login-btn btn btn-orange']) }}
        </p>
        {{ Form::close() }}
    </div>

    <hr>
</div>
@stop
