@extends('backend.layout.master')

@section('title', 'Setting')

@section('contant')
<div class="page-wrapper p-5">
    
    <h1 style="text-align: center;">Edit Setting</h1>
    <div class="">
        @if( $errors->any() )
            <div class="alert alert-danger" style="color: red;">
                @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </div>
        @endif

        @if (\Session::has('success'))
            <div class="alert alert-success" style="color: green">
                {!! \Session::get('success') !!}</li>
            </div>
        @endif

        @if (\Session::has('danger'))
            <div class="alert alert-danger" style="color: red;">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif

        {{ Form::open(['files' => true])  }}
        <p class="">
            {{ Form::label('app_name') }}
            {{ Form::text('app_name', '', ['class' => 'form-control']) }}
        </p>
        <div class="row">        
            <p class="col-6">
                {{ Form::label('logo') }}
                <img src="{{ url('imgs/'.$edit->logo) }}" alt="{{ $edit->app_name }}" width="50">
                {{ Form::file('logo', ['class' => 'form-control','placeholder'=>'Image']) }}
            </p>
            <p class="col-6">
                {{ Form::label('favicon') }}
                <img src="{{ url('imgs/'.$edit->favicon) }}" alt="{{ $edit->app_name }}" width="50">
                {{ Form::file('favicon', ['class' => 'form-control','placeholder'=>'Image']) }}
            </p>
        </div>
        <p>
            {{ Form::label('app_version') }}
            {{ Form::text('app_version', '', ['class' => 'form-control']) }}

        </p>                                
        <p class="">
            {{ Form::label('mobile_no') }}
            {{ Form::number('mobile_no', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('email') }}
            {{ Form::email('email', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('address') }}
            {{ Form::text('address', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('refer_prefix') }}
            {{ Form::text('refer_prefix', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('refer_point') }}
            {{ Form::text('refer_point', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('game_link') }}
            {{ Form::text('game_link', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('about') }}
            {{ Form::textarea('about', '', ['class' => 'form-control']) }}
        </p>
        <h2>Banner</h2>
        <div class="row">        
            <p class="col-8">
                {{ Form::label('banner_title') }}
                {{ Form::text('banner_title', '', ['class' => 'form-control']) }}
            </p>
            <p class="col-4">
                {{ Form::label('banner_image') }}
            <img src="{{ url('imgs/'.$edit->banner_image) }}" alt="{{ $edit->banner_title }}" width="50">
                {{ Form::file('banner_image', ['class' => 'form-control','placeholder'=>'Image']) }}
            </p>
        </div>
        <p class="">
            {{ Form::label('banner_desc') }}
            {{ Form::textarea('banner_desc', '', ['class' => 'form-control']) }}
        </p>
        <h2>Contact</h2>
        <p class="">
            {{ Form::label('contact_page_desc') }}
            {{ Form::textarea('contact_page_desc', '', ['class' => 'form-control']) }}
        </p>
        <h2>Social Links</h2>
        <div class="row">        
            <p class="col-6">
                {{ Form::label('twitter') }}
                {{ Form::text('twitter', '', ['class' => 'form-control']) }}
            </p>
            <p class="col-6">
                {{ Form::label('instagram') }}
                {{ Form::text('instagram','', ['class' => 'form-control']) }}
            </p>
            <p class="col-6">
                {{ Form::label('facebook') }}
                {{ Form::text('facebook','', ['class' => 'form-control']) }}
            </p>
            <p class="col-6">
                {{ Form::label('linkedin') }}
                {{ Form::text('linkedin','', ['class' => 'form-control']) }}
            </p>
        </div>
        <p class="">
            {{ Form::submit('Save', ['class' => 'login-btn btn btn-orange']) }}
        </p>
        {{ Form::close() }}
    </div>

    <hr>
</div>
@stop
