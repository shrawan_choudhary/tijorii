@extends('backend.layout.master')

@section('title','survey histories')

@section('contant')

<div class="page-wrapper p-5">
    <h1 class="text-center my-5">Survey History</h1>


    <div class="">
    @if (\Session::has('success'))
        <div class="alert alert-success" style="color: green">
            {!! \Session::get('success') !!}</li>
        </div>
    @endif

    @if (\Session::has('danger'))
        <div class="alert alert-danger" style="color: red;">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif


    <div class="row">
        <div class="col-lg-6">
            {!! Form::open(['method' => 'GET']) !!}
                <div class="input-group my-3">
                    {!! Form::text('task_id','',['class' => 'form-control', 'placeholder' => 'Search by name...'])!!}
                
                        <select name="status" class="form-control">
                            <option value="">Select Type</option>
                            <option value="completed">Completed</option>
                            <option value="pending">Pending</option>
                        </select>
                        
                    {{ Form::submit('search', ['class'=>'btn btn-primary']) }}
                    </div>
                </div>
            {!! Form::close() !!}
            
            

        <div class="col-lg-6">
            <div class="">
            <form method="GET" action="{{ route('survey_export') }}">
                <div class="input-group my-3">
                    {!! Form::text('task_id','',['class' => 'form-control', 'placeholder' => 'Search by name...'])!!}
                    <select name="status" class="form-control">
                            <option value="">Select Status</option>
                            <option value="completed">Completed</option>
                            <option value="pending">Pending</option>
                        </select>
                        
                    {{ Form::submit('Export csv', ['class'=>'btn btn-primary']) }}
                    </div>
                
            </form>
            </div>
        </div>
        </div>
    </div>
    {{ Form::open( ['url' => url('admin-control/survey-histories/removeMultiple'), 'method'=>'post'] ) }}
    <div class="card">
        <div class="card-body">
            @if( !$lists->isEmpty() )
            <div class="table-responsive">
                <div class="row mb-3">
                    <div class="col float-left" style="font-size: 18px;">{{ $lists->firstItem() }} - {{ $lists->lastItem() }} out of {{ $lists->total() }} record(s) showing.                
                    </div>
                
                    <div class="col text-right">
                        <button type="button" class="btn btn-primary btn-remove"><i class="icon-bin"></i></button>
                    </div>
                </div>
	
    <div class="table-responsive" style="background: #fff;">
        {{ Form::open() }}
        
        <table border="1" style="width: 100%" class="table table-bordered">
            <thead style="font-size: 16px; color: #000;">
                <tr>
                   	<th></th>
                    <th>S.No.</th>
                    <th>Survey Name</th>
                    <th>User Name</th>
                    <th>Status</th> 
                    <th>Click id</th> 
                    <th>Date</th>          
                    <!-- <th>Action</th> -->
                </tr>
            </thead>
            <tbody>
                @php
                $sn = $lists->firstItem();
                @endphp
                @foreach( $lists as $list )
                <tr>
                    <td>{{ Form::checkbox('check[]',$list->id, '',['class'=>'check']) }}</td>
                    <td>{{ $sn++ }}.</td>
                    <td>{{ $list->survey->name ?? "N/A"}} </td>                    
                    <td>{{ $list->user->name ?? "N/A" }}</td>
                    <td>
                         @if($list->status == "completed")
                            <a class="text-success">Completed</a> 
                         @else
                            <a class="text-danger"> Pending</a>
                         @endif
                     </td>  
                     <!-- <td>
                         @if($list->status == "completed")
                            <a class="text-success">Completed</a> 
                         @else
                            <a href="{{ route('change_status', [$list->id, 'field' => 'status', 'status' => 'completed', 'id' => $list->id]) }}" class="text-danger"> Pending</a>
                         @endif
                     </td>  -->                            
                    <td>{{ md5($list->id) }}</td>    
                    <td>{{ $list->updated_at->format('d-M-Y')}}</td>                
                    <!-- <td>
                    	<a href="{{ url('admin-control/survey-histories/remove/'.$list->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">DELETE</a>
                    </td> -->
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ Form::close() }}
    </div>
    @else
    Records Not Found
    {{ $lists->links() }}
    @endif
</div>
@stop