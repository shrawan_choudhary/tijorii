@extends('backend.layout.master')

@section('title','dashboard')

@section('contant')


	<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <a href="{{ url('admin-control/dashboard') }}">
                        <div class="card card-hover">
                            <div class="box bg-cyan text-center" style="height: 135px;">
                                <h1 class="font-light text-white"><i class="mdi mdi-view-dashboard"></i></h1>
                                <h6 class="text-white">Dashboard</h6>
                            </div>
                        </div>
                    </a>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-4 col-xlg-3">
                        <a href="{{ url('admin-control/category') }}">
                            <div class="card card-hover">
                                <div class="box bg-success text-center" style="height: 135px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-file-tree"></i></h1>
                                    <h6 class="text-white">Category</h6>
                                    <h3 class="text-white">{{ count($category) }}</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                     <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <a href="{{ url('admin-control/offer-list') }}">
                            <div class="card card-hover">
                                <div class="box bg-warning text-center" style="height: 135px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-receipt"></i></h1>
                                    <h6 class="text-white">Offer list</h6>
                                    <h3 class="text-white">{{ count($offer) }}</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <a href="{{ url('admin-control/survey') }}">
                            <div class="card card-hover">
                                <div class="box bg-danger text-center" style="height: 135px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-clipboard-text"></i></h1>
                                    <h6 class="text-white">Survey</h6>
                                    <h3 class="text-white">{{ count($survey) }}</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <a href="{{ url('admin-control/faqs') }}">
                            <div class="card card-hover">
                                <div class="box bg-info text-center" style="height: 135px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-comment-question-outline"></i></h1>
                                    <h6 class="text-white">Faqs</h6>
                                    <h3 class="text-white">{{ count($faq) }}</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-md-6 col-lg-4 col-xlg-3">
                        <a href="{{ url('admin-control/pages') }}">
                            <div class="card card-hover">
                                <div class="box bg-danger text-center" style="height: 135px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-google-pages"></i></h1>
                                    <h6 class="text-white">Pages</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <a href="{{ url('admin-control/wallet/credit') }}">
                            <div class="card card-hover">
                                <div class="box bg-cyan text-center" style="height: 135px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-wallet"></i></h1>
                                    <h6 class="text-white">Credit Wallet</h6>
                                    <h3 class="text-white">{{ count($credit) }}</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <a href="{{ url('admin-control/wallet/debit') }}">
                            <div class="card card-hover">
                                <div class="box bg-cyan text-center" style="height: 135px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-wallet"></i></h1>
                                    <h6 class="text-white">Debit Wallet</h6>
                                    <h3 class="text-white">{{ count($debit) }}</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 col-lg-4 col-xlg-3">
                        <a href="{{ url('admin-control/customer-list') }}">
                            <div class="card card-hover">
                                <div class="box bg-success text-center" style="height: 135px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-account-multiple"></i></h1>
                                    <h6 class="text-white">Customer List</h6>
                                    <h3 class="text-white">{{ count($user) }}</h3>
                                </div>
                            </div>
                        </a>
                    </div>                    
                    <!-- Column -->
                     <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <a href="{{ url('admin-control/setting/edit/1') }}">
                            <div class="card card-hover">
                                <div class="box bg-warning text-center" style="height: 135px;">
                                    <h1 class="font-light text-white"><i class=" ti-settings"></i></h1>
                                    <h6 class="text-white">Setting</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                
                <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <a href="{{ url('admin-control/spin/edit/1') }}">
                            <div class="card card-hover">
                                <div class="box bg-info text-center" style="height: 135px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-reload"></i></h1>
                                    <h6 class="text-white">Spin Wheels</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            

@stop