@extends('backend.layout.master')

@section('title','Customer')

@section('contant')

<div class="page-wrapper p-5">
    <h1 class="text-center my-5">Customers</h1>
<div class="">
    @if (\Session::has('success'))
        <div class="alert alert-success" style="color: green">
            {!! \Session::get('success') !!}</li>
        </div>
    @endif

    @if (\Session::has('danger'))
        <div class="alert alert-danger" style="color: red;">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif


    <div class="row">
        <div class="col-lg-3">
            {!! Form::open(['method' => 'GET']) !!}
                <div class="input-group my-3">
                    {!! Form::text('name','',['class' => 'form-control', 'placeholder' => 'Search by type...'])!!}
                    <div class="input-group-append">
                        
                    {{ Form::submit('search', ['class'=>'btn btn-primary']) }}
                    </div>
                </div>
            {!! Form::close() !!}

        </div>
    </div>
    {{ Form::open( ['url' => url('admin-control/customer-list/removeMultiple'), 'method'=>'post'] ) }}
    <div class="card">
        <div class="card-body">
            @if( !$lists->isEmpty() )
            <!-- <div class="table-responsive"> -->
                <div class="row mb-3">
                    <div class="col float-left" style="font-size: 18px;">{{ $lists->firstItem() }} - {{ $lists->lastItem() }} out of {{ $lists->total() }} record(s) showing.                
                    </div>
                    <div class="col text-right">
                        <!-- <button type="submit" class="btn btn-primary" ><i class="icon-bin"></i></button> -->
                    </div>
                </div>
	
    <div class="table-responsive" style="background: #fff;">
        {{ Form::open() }}
        
        <table border="1" style="width: 100%" class="table table-bordered">
            <thead style="font-size: 16px; color: #fff;background: #1d262d;">
                <tr>
                   
                    <th>S.No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Referal_code</th>
                    <th>Gender</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                $sn = $lists->firstItem();
                @endphp
                @foreach( $lists as $list )
                <tr>                    
                    <td>{{ $sn++ }}.</td>
                    <td>{{ $list->name }} </td>                    
                    <td>{{ $list->email }}</td>
                    <td>{{ $list->mobile }}</td>                    
                    <td>{{ $list->referal_code }}</td>
                    <td>{{ $list->gender }}</td>                    
                    <td>{{ $list->role }}</td> 
                    <td>
                         @if($list->status == "Y")
                            <a href="{{ route('change_customer_status', [$list->id, 'field' => 'status', 'status' => 'N', 'id' => $list->id]) }}" class="text-success"> Unblock</a>
                         @else
                            <a href="{{ route('change_customer_status', [$list->id, 'field' => 'status', 'status' => 'Y', 'id' => $list->id]) }}" class="text-danger">Block</a>
                         @endif
                     </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ Form::close() }}
    </div>

    {{ $lists->links() }}
    @else
    No Record Found
    @endif
</div>

@stop