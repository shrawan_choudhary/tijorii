@extends('backend.layout.master')

@section('title','Wallet')

@section('contant')

<div class="page-wrapper p-5">
    <h1 class="text-center my-5">Debit Wallet</h1>
<div class="">
    @if (\Session::has('success'))
        <div class="alert alert-success" style="color: green">
            {!! \Session::get('success') !!}</li>
        </div>
    @endif

    @if (\Session::has('danger'))
        <div class="alert alert-danger" style="color: red;">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif


    <div class="row">
        <div class="col-lg-6">
            {!! Form::open(['method' => 'GET']) !!}
                <div class="input-group my-3">
                    {!! Form::text('uid','',['class' => 'form-control', 'placeholder' => 'Search by name or mobile...'])!!}
                    
                    <div class="input-group-append">
                        
                    {{ Form::submit('search', ['class'=>'btn btn-primary']) }}
                    </div>
                </div>
            {!! Form::close() !!}
            
        </div>
        <div class="col-lg-6">
            <div class="">
            <form method="GET" action="{{ route('debit_export_wallet') }}">
                <div class="input-group my-3">
                    {!! Form::text('uid','',['class' => 'form-control', 'placeholder' => 'Search by name or mobile...'])!!}
                    
                    <div class="input-group-append">
                        
                    {{ Form::submit('Export csv', ['class'=>'btn btn-primary']) }}
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            @if( !$lists->isEmpty() )
            <div class="table-responsive">
                <div class="row mb-3">
                    <div class="col float-left" style="font-size: 18px;">{{ $lists->firstItem() }} - {{ $lists->lastItem() }} out of {{ $lists->total() }} record(s) showing.                
                    </div>
                
                    <div class="col text-right">
                        <!-- <button type="submit" class="btn btn-primary" ><i class="icon-bin"></i></button> -->
                    </div>
                </div>

	
    <div class="table-responsive" style="background: #fff;">
        {{ Form::open() }}
        
        <table border="1" style="width: 100%" class="table table-bordered">
            <thead style="font-size: 16px; color: #000;">
                <tr>
                   
                    <th>S.No.</th>
                    <th>User name</th>
                    <th>Mobile no</th>
                    <th>Offer</th>
                    <th>amount</th>
                    <th>Type</th>
                    <!-- <th>Status</th> -->
                    <th>Remarks</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Pay</th>
                    
                </tr>
            </thead>
            <tbody>
                @php
                $sn = $lists->firstItem();
                @endphp
                @foreach( $lists as $list )
                <tr>
                    
                    <td>{{ $sn++ }}.</td>
                    <td>{{ $list->user->name ?? "N/A"}} </td>
                    <td>{{ $list->user ? $list->user->mobile : "N/A"}} </td>
                    <td>{{ $list->offer }}</td>
                    <td>{{ $list->amount }}</td>
                    <td>{{ $list->type }}</td>
                    <!-- <td style="color: green;">{{ $list->status }}pending/approved</td> -->
                    <td>{{ $list->remarks }}</td>
                    <td>
                         @if($list->status == "pending")
                            <a href="{{ route('change_status_wallet', [$list->id, 'field' => 'status', 'status' => 'approved', 'id' => $list->id]) }}" class="text-danger" >pending </a>
                         @else
                            <a class="text-success">approved</a>
                         @endif
                     </td>
                     <td>{{ $list->created_at->format('d-M-y')}}</td> 
                     <td>
                        
                            

                           
                    <a href="{{ url('admin-control/initiate/'.$list->id) }}" class="btn">pay now</a>        
                     </td>
                    
                    
                </tr>
                @endforeach
            </tbody>
        </table>
                        
                   
        <!-- <span data-href="/tasks" id="export" class="btn btn-success btn-sm" onclick="exportTasks(event.target);">Export</span> -->
        

    </div>

    {{ $lists->links() }}
    @endif
</div>

@stop