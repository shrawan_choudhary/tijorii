@extends('backend.layout.master')

@section('title', 'Edit Pages')

@section('contant')
<div class="page-wrapper p-5">
    <h1 style="text-align: center;">Edit page</h1>
    <div class="">
        @if( $errors->any() )
            <div class="alert alert-danger" style="color: red;">
                @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </div>
        @endif

        @if (\Session::has('success'))
            <div class="alert alert-success" style="color: green">
                {!! \Session::get('success') !!}</li>
            </div>
        @endif

        @if (\Session::has('danger'))
            <div class="alert alert-danger" style="color: red;">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif

        {{ Form::open(['files' => true])  }}

        <p>
            {{ Form::label('title') }}
            <div class="form-control">
                {{ $edit->title }}
            </div>
        </p>
        <p>
            {{ Form::label('slug') }}
            <div class="form-control">
                {{ $edit->slug }}
            </div>
        </p>
        <p class="">
            {{ Form::label('description') }}
            {{ Form::textarea('description', '', ['class' => 'form-control']) }}
        </p>
        
        <p class="">
            {{ Form::submit('Save', ['class' => 'login-btn btn-orange btn']) }}
        </p>
        {{ Form::close() }}
    </div>

    <hr>
</div>
@stop
