@extends('backend.layout.master')

@section('title', 'Pages')

@section('contant')
<div class="page-wrapper p-5">
    <h1 class="text-center pb-5">Pages</h1>

<div class="">
    @if (\Session::has('success'))
        <div class="alert alert-success" style="color: green">
            {!! \Session::get('success') !!}</li>
        </div>
    @endif

    @if (\Session::has('danger'))
        <div class="alert alert-danger" style="color: red;">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif
<div class="card">
    <div class="card-body">
    @if( !$lists->isEmpty() )
        {{ Form::open() }}
        <div class="row my-3">
            <div class="col float-left ml-3">All Entries{{ $count }}</div>
            <div class="col" style="text-align: right;">
                
            </div>
        </div>
        <div class="table-responsive" style="background: #fff;">
            <table border="1" style="width: 100%" class="table table-bordered">
                <thead style="font-size: 16px; color: #fff; background: #1d262d;">
                    <tr>
                       
                        <th>S.No.</th>
                        <th>Title</th>
                        <th>Slug</th>
                        <th>Description</th>
                        
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $sn = $lists->firstItem();
                    @endphp
                    @foreach( $lists as $list )
                    <tr>
                        
                        <td>{{ $sn++ }}.</td>
                        <td>{{ $list->title }} </td>
                        
                        <td>{{ $list->slug }}</td>
                        <td>{{ $list->description }}</td>
                        
                        <td><a href="{{ url('admin-control/pages/edit/'.$list->id) }}">EDIT</a>&nbsp;</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ Form::close() }}
        </div>

    {{ $lists->links() }}
    @else
    No Record Found
    @endif
</div>
<hr>
</div>
@stop
