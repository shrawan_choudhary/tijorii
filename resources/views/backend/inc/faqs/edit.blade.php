@extends('backend.layout.master')

@section('title', 'Edit Faqs')

@section('contant')
<div class="page-wrapper p-5">
    <h1 style="text-align: center;">Edit Faqs</h1>
    <div class="">
        @if( $errors->any() )
            <div class="alert alert-danger" style="color: red;">
                @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </div>
        @endif

        @if (\Session::has('success'))
            <div class="alert alert-success" style="color: green; ">
                {!! \Session::get('success') !!}</li>
            </div>
        @endif

        @if (\Session::has('danger'))
            <div class="alert alert-danger" style="color: red;">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif

        {{ Form::open() }}
        <p class="">
            {{ Form::label('question') }}
            {{ Form::text('question', '', ['class' => 'form-control','placeholder'=>'Questions']) }}
        </p>
        
                                
        <p class="">
            {{ Form::label('answer') }}
            {{ Form::textarea('answer', '', ['class' => 'form-control','placeholder'=>'Answer']) }}
        </p>
        
        <p class="">
            {{ Form::submit('Save', ['class' => 'login-btn btn btn-orange']) }}
        </p>
        {{ Form::close() }}
    </div>

    <hr>
</div>
@stop
