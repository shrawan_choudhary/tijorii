@extends('backend.layout.master')

@section('title', 'faqs')

@section('contant')
<div class="page-wrapper p-5">
    <h1 style="text-align: center;">Faqs</h1>

<div class="">
    @if (\Session::has('success'))
        <div class="alert alert-success" style="color: green">
            {!! \Session::get('success') !!}</li>
        </div>
    @endif

    @if (\Session::has('danger'))
        <div class="alert alert-danger" style="color: red;">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-3">
            {!! Form::open(['method' => 'GET']) !!}
                <div class="input-group mb-3">
                    {!! Form::text('question','',['class' => 'form-control', 'placeholder' => 'Search...'])!!}
                    <div class="input-group-append">
                        
                    {{ Form::submit('search', ['class'=>'btn btn-primary']) }}
                    </div>
                </div>
            {!! Form::close() !!}

        </div>
        <div class="col-lg-9">
             <a href="{{ url('admin-control/faqs/add') }}" class="btn btn-primary add-btn-right float-right"><i class="mdi mdi-plus"></i> Add Faqs</a>
        </div>
    </div>
    {{ Form::open( ['url' => url('admin-control/faqs/removeMultiple'), 'method'=>'post'] ) }}
    <div class="card">
        <div class="card-body">
            @if( !$lists1->isEmpty() )
                <div class="row mb-3">
                    <div class="col float-left" style="font-size: 18px;">{{ $lists1->firstItem() }} - {{ $lists1->lastItem() }} out of {{ $lists1->total() }} record(s) showing.                
                    </div>
                    <div class="col text-right">
                       
                        <button type="button" class="btn btn-primary btn-remove"><i class="icon-bin"></i></button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table border="1" style="width: 100%" class="table table-bordered">
                        <thead style="font-size: 16px; color: #fff; background: #1F262D;">
                            <tr>
                                <th></th>
                                <th>S.No.</th>
                                <th>Question</th>
                                <th>Answer</th>
                                
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $sn = $lists1->firstItem();
                            @endphp
                            @foreach( $lists1 as $list )
                            <tr>
                                <td>{{ Form::checkbox('check[]',$list->id, '',['class'=>'check']) }}</td>
                                <td>{{ $sn++ }}.</td>
                                <td>{{ $list->question }} </td>
                                <td>{{ $list->answer }}</td>
                                

                                <td><a href="{{ url('admin-control/faqs/edit/'.$list->id) }}">EDIT</a>&nbsp;|
                                <a href="{{ url('admin-control/faqs/remove/'.$list->id) }}"onclick="return confirm('Are you sure you want to delete this item?');">DELETE</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
        
        {{ Form::close() }}
    </div>

    {{ $lists1->links() }}
    @else
    No Record Found
    @endif
</div>
<hr>
</div>
@stop
