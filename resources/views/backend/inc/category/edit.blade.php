@extends('backend.layout.master')

@section('title','category')

@section('contant')


		<div class="page-wrapper p-5">

			@if( $errors->any() )
	            <div class="alert alert-danger" style="color: red;">
	                @foreach($errors->all() as $error)
	                    <li>{!! $error !!}</li>
	                @endforeach
	            </div>
	        @endif

	        @if (\Session::has('success'))
	            <div class="alert alert-success" style="color: green">
	                {!! \Session::get('success') !!}</li>
	            </div>
	        @endif

	        @if (\Session::has('danger'))
	            <div class="alert alert-danger" style="color: red;">
	                {!! \Session::get('danger') !!}</li>
	            </div>
	        @endif
	        <h2 style="text-align: center;">Edit Category</h2>

	        {{ Form::open()  }}
		        <p class="">
		            {{ Form::label('name') }}
		            {{ Form::text('name', '', ['class' => 'form-control','placeholder' => 'Category']) }}
		        </p>
		        <p class="">
		            {{ Form::submit('Save', ['class' => 'login-btn btn btn-orange']) }}
		        </p>
		    {{ Form::close() }}

		    

		
	</div>
	@stop

