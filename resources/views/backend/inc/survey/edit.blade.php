@extends('backend.layout.master')
@section('title', 'Edit Survey')
@section('contant')
<div class="page-wrapper p-5">
    <h1 style="text-align: center;">Edit Survey</h1>
    <div class="">
        @if( $errors->any() )
            <div class="alert alert-danger" style="color: red;">
                @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </div>
        @endif
        @if (\Session::has('success'))
            <div class="alert alert-success" style="color: green">
                {!! \Session::get('success') !!}</li>
            </div>
        @endif
        @if (\Session::has('danger'))
            <div class="alert alert-danger" style="color: red;">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif
        {{ Form::open(['files'=>'true'])  }}
        <p class="">
            {{ Form::label('name') }}
            {{ Form::text('name', '', ['class' => 'form-control']) }}
        </p>
        <p>
        {{ Form::label('category') }}
        {{ Form::select('category', $categoryArr, '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('app_description') }}
            {{ Form::textarea('description', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('earn_price') }}
            {{ Form::text('earn_price', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('playstore_link') }}
            {{ Form::text('playstore_link', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('apk_size') }}
            {{ Form::text('apk_size', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('task') }}
            {{ Form::text('task', '', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::label('Type') }}
            {{ Form::select('type', ['survey' => 'survey'],'survey',['class' => 'form-control']) }}
        </p>
        <p class="">
            <img src="{{url('imgs/survey/'.$edit->image)}}" height="50">
            {{ Form::label('image') }}
            {{ Form::file('image', ['class' => 'form-control']) }}
        </p>
        <p class="">
            {{ Form::submit('Save', ['class' => 'login-btn btn btn-orange']) }}
        </p>
        {{ Form::close() }}
    </div>
    <hr>
</div>
@stop
