@extends('backend.layout.master')

@section('title','Customer')

@section('contant')
<div class="page-wrapper p-5">
<div class="container" width="500px">
    <div class="panel panel-primary" style="margin-top:110px;">
        <div class="panel-heading"><h3 class="text-center"></h3></div>
        <div class="panel-body">
            <form action="{{ route('make.payment') }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}

                @if($message = Session::get('message'))
                        <p>{!! $message !!}</p>
                    <?php Session::forget('success'); ?>
                @endif

                <div class="row">
                    <div class="col-md-6">
                        <p class="">
				            {{ Form::label('Name') }}
				            {{ Form::text('uid', '', ['class' => 'form-control']) }}
				        </p>
                    </div>
                    <div class="col-md-6">
                        <p class="">
				            {{ Form::label('Mobile_no') }}
				            {{ Form::text('mobile_no', '', ['class' => 'form-control']) }}
				        </p>
                    </div>
                    <div class="col-md-6">
                        <p class="">
				            {{ Form::label('offer') }}
				            {{ Form::text('offer', '', ['class' => 'form-control']) }}
				        </p>
                    </div>
                    <div class="col-md-6" >
                        
                        <p class="">
				            {{ Form::label('amount') }}
				            {{ Form::text('amount', '', ['class' => 'form-control']) }}
				        </p>
                    </div>
                    <div class="col-md-6" >
                        
                        <p class="">
				            {{ Form::label('email') }}
				            {{ Form::text('email', '', ['class' => 'form-control']) }}
				        </p>
                    </div>
                    <div class="col-md-12">
                        <br/>
                        <button type="submit" class="btn btn-success">Paytm</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@stop