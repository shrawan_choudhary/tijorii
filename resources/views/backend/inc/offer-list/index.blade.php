@extends('backend.layout.master')

@section('title', 'Offers')

@section('contant')
<div class="page-wrapper p-5">
<h2 style="text-align: center;">Offers</h2>
<hr>
<div class="">
    @if (\Session::has('success'))
        <div class="alert alert-success" style="color: green">
            {!! \Session::get('success') !!}</li>
        </div>
    @endif

    @if (\Session::has('danger'))
        <div class="alert alert-danger" style="color: red;">
            {!! \Session::get('danger') !!}</li>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-3">
            {!! Form::open(['method' => 'GET']) !!}
                <div class="input-group mb-3">
                    {!! Form::text('name','',['class' => 'form-control', 'placeholder' => 'Search...'])!!}
                    <div class="input-group-append">
                        
                    {{ Form::submit('search', ['class'=>'btn btn-primary']) }}
                    </div>
                </div>
            {!! Form::close() !!}

        </div>
        <div class="col-lg-9">
            <a href="{{ url('admin-control/offer-list/add') }}" class="btn btn-primary add-btn-right float-right"><i class="mdi mdi-plus"></i> Add Offer</a>
        </div>
    </div>
    {{ Form::open( ['url' => url('admin-control/offer-list/removeMultiple'), 'method'=>'post'] ) }}
    <div class="card">
        <div class="card-body">
            @if( !$lists->isEmpty() )
            <div class="row mb-3">
                <div class="col float-left" style="font-size: 18px;">{{ $lists->firstItem() }} - {{ $lists->lastItem() }} out of {{ $lists->total() }} record(s) showing.                
                </div>
                <div class="col text-right">
                    
                    <button type="button" class="btn btn-primary btn-remove"><i class="icon-bin"></i></button>
                </div>
            </div>
            <div class="table-responsive">
                <table border="1" style="width: 100%" class="table table-bordered">
                    <thead style="font-size: 16px; color: #fff; background: #1F262D;">
                        <tr>
                            <th></th>
                            <th>S.No.</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Category</th>                            
                            <th>Earn_price</th>
                            <th>Playstore link</th>
                            <th>Apk size</th>
                            <th>Task</th>
                            <!-- <th>Type</th> -->
                            <th>Token</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $sn = $lists->firstItem();
                        @endphp
                        @foreach( $lists as $list )
                        <tr>
                            <td>{{ Form::checkbox('check[]',$list->id, '',['class'=>'check']) }}</td>
                            <td>{{ $sn++ }}.</td>
                            <td>{{ $list->name }} </td>
                            <td><img src="{{url('imgs/offer-list/'.$list->image)}}" height="50"></td>
                            <td>{{ $list->category }}</td>
                            <td>{{ $list->earn_price }}</td>
                            <td>{{ $list->playstore_link }}</td>
                            <td>{{ $list->apk_size }}</td>
                            <td>{{ $list->task }}</td>
                            <!-- <td>{{ $list->type }}</td> -->
                            <td>{{ $list->tracking_token }}</td>
                            <td><a href="{{ url('admin-control/offer-list/edit/'.$list->id) }}">EDIT</a>&nbsp;|
                            <a href="{{ url('admin-control/offer-list/remove/'.$list->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">DELETE</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            {{ $lists->links() }}
            @else
            No Record Found
            @endif
        </div>
    </div>
</div>
<hr>
</div>
{{ Form::close() }}
@stop
