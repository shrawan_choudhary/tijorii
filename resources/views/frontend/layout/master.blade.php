@php 
	$setting = App\Model\Setting::findOrFail(1);
	
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{$setting->app_name}}-@yield('title')</title>

  <!-- Favicons -->
  <link href="{{ url('imgs/'.$setting->favicon) }}" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- Vendor CSS Files -->lo
  
	{{Html::style('vendor/bootstrap/css/bootstrap.min.css')}}
	{{Html::style('vendor/icofont/icofont.min.css')}}
	{{Html::style('vendor/owl.carousel/assets/owl.carousel.min.css')}}
	{{Html::style('vendor/boxicons/css/boxicons.min.css')}}
	{{Html::style('vendor/venobox/venobox.css')}}
	{{Html::style('vendor/aos/aos.css')}}
	{{Html::style('https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')}}
	{{Html::style('css/style.css')}}
</head>

<body>

	@section('header')
		<header id="header" class="fixed-top">
			<div class="container-fluid d-flex justify-content-between align-items-center">
			    <a href="{{ url('/') }}">
    			    @if($setting->logo!='')
    				<img src="{{ url('imgs/'.$setting->logo) }}" alt="{{ $setting->app_name }}" width="80">
    			    @else
    			    <h1 class="logo"><a href="{{ url('/') }}">{{ $setting->app_name }}</a></h1>
    			    @endif
    			</a>
			<!-- Uncomment below if you prefer to use an image logo -->
			<!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

			<nav class="nav-menu d-none d-lg-block">
				<ul>
				<li class="active"><a href="{{ url('/') }}">Home</a></li>
				<li><a href="{{ url('/about-us') }}">About</a></li>
				<li><a href="{{ url('/contact') }}">Contact</a></li>
				</ul>
			</nav><!-- .nav-menu -->

			<div class="header-social-links">
				<a href="{{ $setting->twitter }}" class="twitter" target="_blank"><i class="icofont-twitter"></i></a>
				<a href="{{ $setting->facebook }}" class="facebook" target="_blank"><i class="icofont-facebook"></i></a>
				<a href="{{ $setting->instagram }}" class="instagram" target="_blank"><i class="icofont-instagram"></i></a>
				<a href="{{ $setting->linkedin }}" class="linkedin" target="_blank"><i class="icofont-linkedin"></i></i></a>
			</div>

			</div>

		</header>
	@show

		@yield('contant')

	@section('footer')
	 <footer class="color-footer-m">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-sm-6">
        <div class="footer-widget about-widget">
          <h2>About</h2>
          <p>{{ $setting->about }}</p>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6">
        <div class="footer-widget about-widget">
          <h2>Privacy Policy</h2>
          <ul class="mar-l">
           <li><a href="{{ url('/about-us') }}">About Us</a></li>
           <li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
           <li><a href="{{ url('/term-condition') }}">Term & Condition</a></li>
          </ul>
          <!-- <ul>
            <li><a href="#">Support</a></li>
            <li><a href="#">Terms of Use</a></li>
          </ul> -->
        </div>
      </div>
      <!-- <div class="col-lg-4 col-sm-6">
        <div class="footer-widget about-widget">
          <h2>Terms & Conditions</h2>
          <div class="fw-latest-post-widget">
          </div>
        </div>
      </div> -->
      <div class="col-lg-4 col-sm-6">
        <div class="footer-widget contact-widget">
          <h2>Get in Touch</h2>
          <!-- <div class="con-info">
            <span><i class="fa fa-home"></i></span>
            <p> Kumawat Enterprises </p>
          </div> -->
          <div class="con-info">
            <span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
            <p>{{ $setting->address }}</p>
          </div>
          <div class="con-info">
            <span><i class="fa fa-phone" aria-hidden="true"></i></span>
            <p>{{ $setting->mobile_no }}</p>
          </div>
          <div class="con-info">
            <span><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
            <p><a href="mailto:{{ $setting->email }}" style="color: #f5e3ea;">{{ $setting->email }}</a></p>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</footer>
	@show
<div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="bx bx-up-arrow-alt"></i></a>

  <!-- Vendor JS Files -->
  {{Html::script('vendor/jquery/jquery.min.js')}}
  {{Html::script('vendor/bootstrap/js/bootstrap.bundle.min.js')}}
  {{Html::script('vendor/jquery.easing/jquery.easing.min.js')}}
  {{Html::script('vendor/php-email-form/validate.js')}}
  {{Html::script('vendor/waypoints/jquery.waypoints.min.js')}}
  {{Html::script('vendor/counterup/counterup.min.js')}}
  {{Html::script('vendor/owl.carousel/owl.carousel.min.js')}}
  {{Html::script('vendor/isotope-layout/isotope.pkgd.min.js')}}
  {{Html::script('vendor/venobox/venobox.min.js')}}
  {{Html::script('vendor/aos/aos.js')}}
  {{Html::script('js/main.js')}}
<script>
    $(function(){
    
    
$("#enquiry_form").on('submit', function(e){
    e.preventDefault();
    
    var url =  $(this).attr('data-url');
    var name = $('#enquiry_name').val();
    var email = $('#enquiry_email').val();
    var subject = $('#enquiry_subject').val();
    var message = $('#enquiry_message').val();
    
   $.ajax({
          url:url,
          type:"POST",
          headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data:{
            name:name,
            email:email,
            subject:subject,
            message:message
          },
          success:function(response){
              
              if(response.status){
                  document.getElementById("enquiry_form").reset();
                   $("#ajax_message").html();
                  $("#ajax_message").html("Inquiry Send Successfully");
              }
            else{
                 $("#ajax_message").html();
                 $("#ajax_message").html('failed');
            }
            
          },
    });
});
    
})
</script>
</body>

</html>