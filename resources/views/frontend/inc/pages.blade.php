@extends('frontend.layout.master')

@section('title', $pages->title)

@section('contant')
	<main id="main">

    <!-- ======= About Section ======= -->
  <section id="about" class="about">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>{{ $pages->title }}</h2>
        <p>{{ $pages->description }}</p>
      </div>

      </div>
  </section><!-- End About Section -->

  </main><!-- End #main -->
@stop