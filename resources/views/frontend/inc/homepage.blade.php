@extends('frontend.layout.master')

@section('title','Homepage')

@section('contant')
	<!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center" style="background: url('{{ url('imgs/'.$setting->banner_image)}}') top right;">
    <div class="container d-flex flex-column align-items-center" data-aos="zoom-in" data-aos-delay="100">
      <h1>{{ $setting->banner_title }}</h1>
      <h2>{{ $setting->banner_desc }}</h2>
      <a href="{{ url('/about-us') }}" class="btn-about">About Me</a>
    </div>
  </section><!-- End Hero -->
@stop