<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Spin Wheel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            .screen_wrapper {
                margin: auto;
                background: #fff;
                height: 100%;
                overflow: hidden;
            }
            .spinner {
                position: relative;
                display: flex;
                align-items: center;
                justify-content: center;
                height: 100%;
            }
            .pieBackground {
              position: absolute;
              width: 250px;
              height: 250px;
              border-radius: 100%;
              border: 10px solid #f1c40f;
            }
            .pie {
              transition: all 1s;
              position: absolute;
              width: 250px;
              height: 250px;
              border-radius: 100%;
              clip: rect(0px, 125px, 250px, 0px);
              transform: rotate(36deg);
            }

            .hold {
              position: absolute;
              width: 250px;
              height: 250px;
              border-radius: 100%;
              clip: rect(0px, 250px, 250px, 125px);
            }

            #pieSlice1 .pie {
              background-color: #1abc9c;
              transform:rotate(36deg);
            }

            #pieSlice2 {
              transform: rotate(36deg);
            }
            #pieSlice2 .pie {
              background-color: #3498db;
            }

            #pieSlice3 {
              transform: rotate(72deg);
            }
            #pieSlice3 .pie {
              background-color: #9b59b6;
            }

            #pieSlice4 {
              transform: rotate(108deg);
            }
            #pieSlice4 .pie {
              background-color: #34495e;
            }

            #pieSlice5 {
              transform: rotate(144deg);
            }
            #pieSlice5 .pie {
              background-color: #e67e22;
            }

            #pieSlice6 {
              transform: rotate(180deg);
            }
            #pieSlice6 .pie {
              background-color: #1abc9c;
            }

            #pieSlice7 {
              transform: rotate(216deg);
            }
            #pieSlice7 .pie {
              background-color: #3498db;
            }

            #pieSlice8 {
              transform: rotate(252deg);
            }
            #pieSlice8 .pie {
              background-color: #9b59b6;
            }

            #pieSlice9 {
              transform: rotate(288deg);
            }
            #pieSlice9 .pie {
              background-color: #34495e;
            }

            #pieSlice10 {
              transform: rotate(324deg);
            }
            #pieSlice10 .pie {
              background-color: #e67e22;
            }

            .innerCircle {
              position: absolute;
              width: 120px;
              height: 120px;
              background-color: #444;
              border-radius: 100%;
              top: 15px;
              left: 15px;
              box-shadow: 0px 0px 8px rgba(0,0,0,0.5) inset;
              color: white;
            }
            .innerCircle .content {
              position: absolute;
              display: block;
              width: 120px;
              top: 30px;
              left: 0;
              text-align: center;
              font-size: 14px;
            }
            .spin-button {
                position: absolute;
                top: 50%;
                left: 50%;
                width: 50px;
                height: 50px;
                background: #fff;
                text-align: center;
                line-height: 50px;
                border-radius: 50%;
                font-weight: bold;
                cursor: pointer;
                border: 5px solid #f1c40f;
                transform: translate(-50%, -50%);
                box-shadow: 0 0 10px #000;
            }
            .spin-button::after {
                content: "";
                position: absolute;
                border-width: 25px 15px;
                border-style: solid;
                border-color: transparent transparent #f1c40f transparent;
                top: -50px;
                left: 50%;
                transform: translateX(-50%);
            }
            @keyframes rotateSpin {
                from {
                    transform: translate(-50%, -50%) rotate(0deg);
                }
                to {
                    transform: translate(-50%, -50%) rotate(359deg);
                }
            }
            @-webkit-keyframes rotateSpin {
                from {
                    -webkit-transform: translate(-50%, -50%) rotate(0deg);
                }
                to {
                    -webkit-transform: translate(-50%, -50%) rotate(359deg);
                }
            }
            .spinContainer {
                position: absolute;
                left: 50%;
                top: 50%;
                transform: translate(-50%, -50%) rotate(-18deg);
                width: 250px;
                height: 250px;
                border-radius: 50%;
                transition: transform 5s cubic-bezier(0.01, 0.15, 0.01, 1);
            }
            .spinContainer.active {
                -webkit-transform: translate(-50%, -50%) rotate(359deg);
            }
            .number {
                position: absolute;
                color: #fff;
                left: 50%;
                top: 50%;
                transform: translate(-50%, -50%) rotate(-18deg);
                margin-top: -100px;
                margin-left: -33px;
                font-size: 24px;
                font-weight: bold;
            }
            .popup-bg, .popup-body {
                position: fixed;
                z-index: 99;
            }
            .popup-bg {
                left: 0;
                right: 0;
                bottom: 0;
                top: 0;
                cursor: not-allowed;
            }
            .popup-body {
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                background: rgba(0, 0, 0, 0.9);
                color: #fff;
                border-radius: 16px;
                padding: 15px;
                text-align: center;
                white-space: nowrap;
            }
        </style>
    </head>
    <body>
        <input type="hidden" id="audioUrl" value="{{ url('audio/spin.mp3') }}">
        <audio src="#" id="bgAudio"></audio>
        <div class="screen_wrapper">
            <div class="spinner">
                <div class="pieBackground"></div>
                <div class="spinContainer">
                    @foreach($spinArr as $i => $num)
                    <div id="pieSlice{{ $i + 1 }}" class="hold">
                        <div class="pie">
                            <div class="number">
                                {{ $num }}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="spin-button">
                    SPIN
                </div>
            </div>
        </div>

        <div class="overlay" @if(!$completed) style="display: none;" @endif>
            <div class="popup-bg"> </div>
            <div class="popup-body">
                <h3>Your Today Task is Completed</h3>
                <p>Please Come Tomorrow To Spin</p>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>
        <script type="text/javascript">
        $(function() {
            let duration    = 5000,
                totalRound  = 10,
                count       = totalRound * 360,
                audioUrl    = $('#audioUrl').val(),
                spinNumbers = {!! json_encode( $spinArr ) !!},
                ascNumbers  = {!! json_encode( $asc_arr ) !!};

            $('.spin-button').on('click', function() {
                $('#bgAudio').attr('src', audioUrl);
                document.getElementById('bgAudio').play();

                let degreeArr = [
                    18,
                    54,
                    90,
                    126,
                    162,
                    198,
                    234,
                    270,
                    306,
                    342
                ];
                let day1_arr = [],  day_arr = [];

                for(let i = 0; i < 10; i++) {
                    let deg = 10 - i - 1;
                    if((spinNumbers[deg] ==  ascNumbers[4] ||  spinNumbers[deg] ==  ascNumbers[5] || spinNumbers[deg] ==  ascNumbers[6]) && spinNumbers[deg] != {{ $lastNumber }} ) {
                        day1_arr.push( i );
                    }
                    if( (spinNumbers[deg] ==  ascNumbers[0] || spinNumbers[deg] ==  ascNumbers[1] || spinNumbers[deg] ==  ascNumbers[2] || spinNumbers[deg] ==  ascNumbers[3]) && spinNumbers[deg] != {{ $lastNumber }} ) {
                        day_arr.push( i );
                    }
                }

                let new_day = {{ $new_day }};
                let degree = parseInt(new_day) == 1 ? day1_arr[parseInt( Math.floor((Math.random() * day1_arr.length)) )] :  day_arr[parseInt( Math.floor((Math.random() * day_arr.length)) )];

                let rotate = degreeArr[degree] + parseInt( count );

                $('.spinContainer').css({
                    'transform': 'translate(-50%, -50%) rotate('+rotate+'deg)',
                });

                count += totalRound * 360;

                let winNum = spinNumbers[10 - degree - 1];

                $.ajax({
                    url: "{{ url('spin-wheel/save') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        point: winNum,
                        uid: {{ $user->id }},
                        day_count: {{ $new_day }}
                    },
                    success: function(res) {
                        console.log(res);
                    }
                });

                setTimeout( () => {
                    document.getElementById('bgAudio').pause();
                    $('#bgAudio').removeAttr('src');
                    alert("You've earned "+winNum+" points.");

                    $('.overlay').show();
                }, duration );
            });
        })
        </script>
    </body>
</html>
